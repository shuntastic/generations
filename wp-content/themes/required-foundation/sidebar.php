<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.1.0
 */
?>
        <!-- START: sidebar.php -->
		<div id="secondary" class="widget-area" role="complementary">
            <div id="column1-wrap">
                <div id="column1"><?php dynamic_sidebar( 'sidebar-left' )  ?></div>
            </div>
            <div id="column2"><?php  dynamic_sidebar( 'sidebar-right' )  ?></div>
            <div id="clear"></div>
		</div><!-- #secondary .widget-area -->
        <!-- END: sidebar.php -->