<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the class=container div and all content after
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.1.0
 */
?>
		<?php
			/*
				A sidebar in the footer? Yep. You can can customize
				your footer with three columns of widgets.
			*/
			if ( ! is_404() )
				get_sidebar( 'footer' );
			?>
			<div id="footer" class="row" role="contentinfo">
				<div id="legal-nav" class="twelve columns">
					<?php wp_nav_menu( array(
						'theme_location' => 'secondary',
						'container' => false,
						'menu_class' => 'inline-list',
						'fallback_cb' => false
					) ); ?>
				</div>
                <div id="legal-copy">
                <p>To be eligible for these products and services, you must live, work, worship, volunteer or attend school in Bexar County, Texas. You are also eligible for membership if you’re family of a current member. To be eligible for these services and products your business must be located in Bexar County, Texas.</p>
                </div>
				<div id="legal-logos" class="twelve columns">
				</div>
			</div>
	</div><!-- Container End -->

	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	     chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7]>
		<script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->
	<!-- <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51d353fb7fb5f49b"></script> 
	<script src="http://maps.google.com/maps/api/js?v=3.2&sensor=false"></script>-->

	<?php wp_footer(); ?>
</body>
</html>