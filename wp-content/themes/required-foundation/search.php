<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.1.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page search" role="main">
			<?php	
				$blogID = 531;	
				$file_info = simple_fields_value("mainvis",$blogID);

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        </div><!-- .entry-content -->
                        <div class="info">
							<?php wp_nav_menu( array(
                                'theme_location' => 'meta',
                                'container' => 'ul',
                                'menu_class' => 'secondary-left-sidebar',
                                'fallback_cb' => false
                            ) ); ?>
                            <div id="main-content" class="nowidget">
								<?php if ( have_posts() ) : ?>
								<div class="section">
									<?php required_archive_title(); ?>

									<?php /* Start the Loop */ ?>
									<?php while ( have_posts() ) : the_post(); ?>
                                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                                                <div>
                                                    <div class="copy nothumb">
                                                    	<h2><?php the_title(); ?></h2>
                                                        <?php 
                                                        	$introcopy = simple_fields_value('introcopy');
                                                        	if($introcopy) : ?>
                                                               <div class="excerpt">
                                                               	<p><?php echo $introcopy; ?><br />
	                                                            <a class="read-more" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">READ MORE</a></p>
	                                                        </div>
                                                            <?php else : ?>
	                                                         	<div class="excerpt"><?php the_excerpt(); ?></div>
                                                            <?php endif; ?>
                                                     </div>
                                                </div>
                                            </div>                 

									<?php endwhile; ?>

								<?php else : ?>
									<?php get_template_part( 'content', 'none' ); ?>
								<?php endif; ?>

								<?php if ( function_exists( 'required_pagination' ) ) {
									required_pagination();
								} ?>
								</div>
                            </div>                 
                </div>
            </div>
		</div><!-- /#main -->
	</div><!-- End Content row -->
            <?php get_footer(); ?>
