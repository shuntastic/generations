<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns section-landing-page" role="main">
			<?php
                $blogID = 9;  
                $file_info = simple_fields_value("mainvis",$blogID);
/*                $page_headline = simple_fields_value("headline",$blogID);
                $intro_copy = simple_fields_value("introcopy",$blogID);
*/
                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2>404 Error:</h2>
                            <p>The page you are looking for could not be found.</p>
                        </div><!-- .entry-content -->
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
