<?php
/**
 * Template Name: Events Page w/Form Template
 * Description: The main template for the Events page
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */
?>
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="robots" content="noindex, nofollow">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<?php
    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
?>
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" />
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/selectivizr.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>
</head>
<body <?php body_class(); ?>>
    <!-- Row for main content area -->
        <div class="row logoheader">
            <header id="nav-header" class="twelve columns required-header" role="banner">
                <a class="header-image" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="/assets/cropped-header-logo.png" alt="" /></a>
            </header>
        </div><!-- // row header -->
<div id="content" class="row form">
    <div id="main" class="twelve columns nonav-page feclasses template-two" role="main">
        <div id="mainvis"></div>
        <div class="post-box">
                <?php   
                    $phonenum = simple_fields_value("phonenumber");
                    if($phonenum) {
                        echo '<div class="phonecallout orangebox">';
                        echo $phonenum;
                        echo ' </div>';
                    }
                ?>
                <div class="promoarea">
                    <div id="promoslide" class="entry-content">
                        <div class="slide">
                                <?php 
                                    $currID = $post->ID;
                                    $file_info = simple_fields_value("nonavpromoimage");
                                    $promo_head = simple_fields_value("nonavpromoheadline");
                                    $promo_copy = simple_fields_value("nonavpromocopy");
                                    $img = wp_get_attachment_image_src($file_info, 'full');
                                    if ($img) {
                                        echo '<div class="imgHolder" style="background:transparent url('.$img[0].') center center no-repeat;background-size:cover;"></div>';
                                    }
                                    echo '<div class="copy">';
                                    the_title('<h4>', '</h4>' );
                                    // if ($promo_head) {
                                    //     echo '<h4>'.$promo_head.'</h4>';
                                    // }
                                    if ($promo_copy) {
                                        echo '<p>'.$promo_copy.'</p>';
                                    }
                                    echo '</div>';
                                ?>
                        </div>
                    </div><!-- .entry-content -->
                 </div>
                   <?php
                        $formHeadline = simple_fields_value("formheadline");
                        $formDesc = simple_fields_value("formdescription");
                        $form = simple_fields_value("formurl");
                        function curPageURL() {
                            $pageURL = 'http';
                            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
                            $pageURL .= "://";
                            if ($_SERVER["SERVER_PORT"] != "80") {
                                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
                            } else {
                                $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                            }
                            return $pageURL;
                        }

                        if($form) {
                            echo '<div class="formholder whitebox">';
                            if($formHeadline) echo '<h2>'.$formHeadline.'</h2>';
                            if($formDesc) echo '<p>'.$formDesc.'</p>';
                            if($form) echo '<iframe src="'.$form.'?parenturl='. curPageURL().'" height="500" width="350"></iframe>';
                            echo '</div>';
                        }
                    ?>
                <div class="onecol">
                    <ul class="bulletlist">
                       <!--  <li><h2 class="event-label event-label-name"><?php _e('Event:', 'tribe-events-calendar'); ?></h2>
                        <p itemprop="name" class="event-meta event-meta-name"><?php the_title(); ?></p></li> -->
                        <?php if (tribe_get_start_date() !== tribe_get_end_date() ) { ?>
                            <li><h2 class="event-label event-label-start"><?php _e('Date:', 'tribe-events-calendar'); ?></h2> 
                            <p class="event-meta event-meta-start"><meta itemprop="startDate" content="<?php echo tribe_get_start_date( null, false, 'Y-m-d-h:i:s' ); ?>"/><?php echo tribe_get_start_date(); ?>&nbsp;-&nbsp;<meta itemprop="endDate" content="<?php echo tribe_get_end_date( null, false, 'Y-m-d-h:i:s' ); ?>"/><?php echo tribe_get_end_date( null, false, 'h:i' ); ?></p></li>
                        <?php } else { ?>
                            <li><h2 class="event-label event-label-date"><?php _e('Date:', 'tribe-events-calendar'); ?></h2> 
                            <p class="event-meta event-meta-date"><meta itemprop="startDate" content="<?php echo tribe_get_start_date( null, false, 'Y-m-d-h:i:s' ); ?>"/><?php echo tribe_get_start_date(); ?></p></li>
                        <?php } ?>
                      <?php if (tribe_get_organizer()): ?>
                            <li><h2 class="event-label event-label-organizer"><?php _e('Organizer:', 'tribe-events-calendar'); ?></h2>
                            <p class="vcard author event-meta event-meta-author"><span class="fn url"><?php echo tribe_get_organizer(); ?></span>
                            <?php if ( tribe_get_organizer_email() ) : ?>
                                <br /><span itemprop="email" class="event-meta event-meta-email"><a href="mailto:<?php echo tribe_get_organizer_email(); ?>"><?php echo tribe_get_organizer_email(); ?></a></span>
                            <?php endif; ?>
                            <?php if ( tribe_get_organizer_phone() ) : ?>
                                <br /><span itemprop="telephone" class="event-meta event-meta-phone"><?php echo tribe_get_organizer_phone(); ?></span>
                            <?php endif; ?>
                        </p></li>
                        <?php endif; ?>
                        <!-- <li><h2 class="event-label event-label-updated"><?php _e('Updated:', 'tribe-events-calendar'); ?></h2>
                        <p class="event-meta event-meta-updated"><span class="date updated"><?php the_date(); ?></span></p></li> -->
                        <?php if ( class_exists('TribeEventsRecurrenceMeta') && function_exists('tribe_get_recurrence_text') && tribe_is_recurring_event() ) : ?>
                            <li><h2 class="event-label event-label-schedule"><?php _e('Schedule:', 'tribe-events-calendar'); ?></h2>
                         <p class="event-meta event-meta-schedule"><?php echo tribe_get_recurrence_text(); ?> 
                            <?php if( class_exists('TribeEventsRecurrenceMeta') && function_exists('tribe_all_occurences_link')): ?>(<a href='<?php tribe_all_occurences_link(); ?>'>See all</a>)<?php endif; ?>
                         </p></li>
                        <?php endif; ?>
                        <?php if ( tribe_get_cost() ) : ?>
                            <li><h2 class="event-label event-label-cost"><?php _e('Cost:', 'tribe-events-calendar'); ?></h2>
                            <p itemprop="price" class="event-meta event-meta-cost"><?php echo tribe_get_cost(); ?></p></li>
                        <?php endif; ?>
                        <?php if(tribe_get_venue()) : ?>
                        <li><h2 class="event-label event-label-venue"><?php _e('Location:', 'tribe-events-calendar'); ?></h2> 
                            <p itemprop="name" class="event-meta event-meta-venue"><?php echo tribe_get_venue( get_the_ID() ); ?>
                        </p></li>
                        <?php if( tribe_address_exists( get_the_ID() ) ) : ?>
                        <li><h2 class="event-label event-label-address"><?php _e('Address:', 'tribe-events-calendar') ?></h2>
                        <?php echo tribe_get_full_address( get_the_ID() ); ?><?php endif; ?>
                        <?php if(tribe_get_phone()) : ?><span itemprop="telephone" class="event-meta event-meta-venue-phone"><?php echo tribe_get_phone(); ?></span><?php endif; ?>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <?php   
                    $contentOutput = simple_fields_values('nonavhtml');
                    if ($contentOutput) {
                        echo '<div class="contentarea">';
                        $ctr = 1;
                        foreach ($contentOutput as $value) {
                            echo '<article class="sect'.$ctr.'">'.$value.'</article>';
                            $ctr++;
                        }
                        echo '</div>';
                    } 
                ?>
            <?php //if ($mapFlag) : ?>
               <?php if( tribe_embed_google_map( get_the_ID() ) ) : ?>
                <hr />
                <div class="maparea whitebox">
                    <div class="map-controls">
                        <?php if( tribe_address_exists( get_the_ID() ) ) : ?>
                        <div class="venue-label venue-label-address">
                            <?php _e('Address:', 'tribe-events-calendar-pro') ?><br />
                            <?php if( get_post_meta( get_the_ID(), '_EventShowMapLink', true ) == 'true' ) : ?>
                                <a class="gmap" itemprop="maps" href="<?php echo tribe_get_map_link(); ?>" title="<?php _e('Click to view a Google Map', 'tribe-events-calendar-pro'); ?>" target="_blank"><?php _e('Google Map', 'tribe-events-calendar-pro' ); ?></a>
                            <?php endif; ?>
                        </div>
                            <div class="venue-meta venue-meta-address">
                            <?php echo tribe_get_full_address( get_the_ID() ); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="mapholder">
                        <!-- <iframe src="http://stage2.mygenfcu.org/locations-map-frame/" width="880" height="410"></iframe> -->
                        <?php if( tribe_address_exists( get_the_ID() ) ) { echo tribe_get_embedded_map(); } ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php   
            $disclaimerOutput = simple_fields_value('disclaimercopy');
            if ($disclaimerOutput) {
                echo '<div class="lpdisclaimer"><p class="disclaimer">'.$disclaimerOutput.'</div>';
            }
        ?>
       </div>
    </div>
    </div>
    </div><!-- End Content row -->
 <?php get_footer(); ?>
