<?php
/**
 * Template Name: Author Page Template
 * Description: The main template for the author page
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page" role="main">
			<?php
				$blogID = 531;	
				$file_info = simple_fields_value("mainvis",$blogID);
				$page_headline = simple_fields_value("headline",$blogID);
				$intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <div class="row blognav">
								<?php wp_nav_menu( array(
                                    'menu' => 13,
                                    'container' => false,
                                    'menu_class' => 'inline-list left',
                                    'fallback_cb' => false
                                ) ); ?>
                            </div>
                        </div><!-- .entry-content -->
                        <div class="info">
                            <div id="main-content" class="nonav">
                                <div class="contributors">
                                    <?php
                                        $authorOutput = simple_fields_values("authorimage,authorname,authordescription,authorID"); 
                                        if($authorOutput) {
    //                                                  $subctr = 1;
                                            foreach ($authorOutput as $value) {
                                                $thumb = $value['authorimage'];
                                                $authorname = $value['authorname'];
                                                $authordesc = $value['authordescription'];
                                                $authorID = $value['authorID'];
                                                $authorURL = get_author_posts_url($authorID);
                                                echo '<div class="section">';
                                                 $image_info1 = wp_get_attachment_image_src($thumb, "full");
                                                echo '<img src="'.$image_info1[0].'" width="140" height="140" />';
                                                echo '<div class="copy">';
                                                echo '<h2>'.$authorname.'</h2>';
                                                echo '<p>'.$authordesc.'</p>';
                                                echo '<a href="'.$authorURL.'">VIEW POSTS</a></div></div>';
    //                                                        $subctr++;
                                            }
                                        }
                                   ?>
                                </div>

                            </div>                 
							<?php if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>