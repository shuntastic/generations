<?php
/**
 * Template Name: Form Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script type="text/javascript">
	 var mktoPreFillFields = {"Email":null,"FirstName":null,"LastName":null,"LeadSource":null,"Phone":null,"Comments__c":null,"RecordTypeId":null,"W2L_Page_Source__c":null};
	</script>
	<script type="text/javascript">
	function fieldValidate(field) {
	  /* call Mkto.setError(field, message) and return false to mark a field value invalid */
	  /* return 'skip' to bypass the built-in validations */
	  return true;
	}
	function getRequiredFieldMessage(domElement, label) {
	  return "This field is required";
	}
	function getTelephoneInvalidMessage(domElement, label) {
	  return "Please enter a valid telephone number";
	}
	function getEmailInvalidMessage(domElement, label) {
	  return "Please enter a valid email address";
	}
	</script>
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/jquery.validate.min.js"></script>
   <script>
    // to make fancy buttons.  Uses noConflict just in case
     var $jQ = jQuery.noConflict();

     // Use jQuery via $j(...)
     $jQ(document).ready(function(){

       $jQ("#mktFrmSubmit").wrap("<div class='buttonSubmit'></div>");
       $jQ(".buttonSubmit").prepend("<span></span>");

     });
   </script>
</head>
<body <?php body_class(); ?> style="background:transparent;">
	<!-- Row for main content area -->
<script type="text/javascript"> 
var profiling = {
  isEnabled: false,
  numberOfProfilingFields: 3,
  alwaysShowFields: [ 'mktDummyEntry']
};
</script>
<!-- <script type="text/javascript"> function mktoGetForm() {return document.getElementById('mktForm_1004'); }</script> -->
<div id="content" class="row form">
	<div class="marketoform">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php the_content(); ?></h2>
                        </div><!-- .entry-content -->
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
	</div>
	</div><!-- End Content row -->
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/mktFormSupport.js"></script>
	<script type="text/javascript">
		function formSubmit(elt) {
		  return Mkto.formSubmit(elt);
		}
		function formReset(elt) {
		  return Mkto.formReset(elt);
		}
	</script>
<!-- GOOGLE ANALYTICS.  ENTER YOUR CODE and UNCOMMENT block -->
<!--
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-18664312-1";
urchinTracker();
</script>
-->
</body>
</html>