<?php
/**
 * Template Name: News and Alerts Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page newsalerts" role="main">
			<?php	
				$file_info = simple_fields_value("mainvis");
				$page_headline = simple_fields_value("headline");
				$intro_copy = simple_fields_value("introcopy");
                if ($file_info) {
                    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                        // if IE<=8, add behavior: url(/backgroundsize.htc);
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';
                    } else {
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
                    }

                }
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
				<?php
                $displayposts = new WP_Query();
                $displayposts->query('cat=8&orderby=date&order=DESC');
                ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                            <?php //the_content(); ?>
                        </div><!-- .entry-content -->
                        <div class="info">
							<?php wp_nav_menu( array(
                                'theme_location' => 'meta',
                                'container' => 'ul',
                                'menu_class' => 'secondary-left-sidebar',
                                'fallback_cb' => false
                            ) ); ?>
                            <div id="main-content" class="nowidget">
                            	<div class="section">
									<h2><?php the_title(); ?></h2><br />
									<?php while ($displayposts->have_posts()) : $displayposts->the_post(); ?>
                                        <?php 
                                            $posttags = wp_get_post_terms( get_the_ID() , 'post_tag' , 'fields=names' );
                                            if( $posttags ) {
                                                $pTags =  implode( ' ' , $posttags );
                                            } 
                                        ?>
                                        <div class="entry">
                                            <?php //$my_date = the_date('M d', '<span class="wp-cpl-date '.$pTags.'">', '</span>', FALSE); echo $my_date; ?>
                                            <?php $my_date = get_the_date('M d'); 
                                                echo '<span class="wp-cpl-date '.$pTags.'">'.$my_date.'</span>';
                                            ?>
                                            <div class="copy">
                                            	<h2 class="alert"><?php the_title(); ?></h2>
                                                <p>
                                                    <?php echo excerpt(50); ?>
                                                    <a class="read-more" href="<?php the_permalink(); ?>"> READ MORE</a>
                                                </p>
                                           		<?php //the_excerpt('read more...'); ?>
                                            </div>
                                        </div>
                                    <?php endwhile; // end of the loop. ?>
                                </div>
                            </div>                 
							<?php //if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
<!--                                 <div class="secondary-right-sidebar widget-area">
                                    <?php // dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                                </div>--> <!-- #first .widget-area -->
                            <?php// endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_footer(); ?>
