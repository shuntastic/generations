<?php
/**
 * Template Name: Section Landing Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns section-landing-page" role="main">
			<?php	
				$file_info = simple_fields_value("mainvis");
				$page_headline = simple_fields_value("headline");
				$intro_copy = simple_fields_value("introcopy");

                if ($file_info) {
                    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                        // if IE<=8, add behavior: url(/backgroundsize.htc);
                    echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';

                    } else {
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
                    }
                }
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                        </div><!-- .entry-content -->
                        <div class="info">
							<?php	
								$landingSections = simple_fields_values('sectiontitle,sectiondescription,sectionurl');
								//foreach ($tabOutput as $values) {
								//	echo "image has id: " . $values["slug_image"] . " and description " . $values["slug_description"];
								//}

                               // $ = simple_fields_get_post_group_values($currID, "Page Tabs", false, 2); 
                                if ($landingSections) {
                                    $ctr = 1;
                                    foreach ($landingSections as $value) {
                                        $sectionTitle = $value['sectiontitle'];
                                        $sectionDesc = $value['sectiondescription'];
                                        $sectionURL = $value['sectionurl'];
										echo '<section class="sect'. $ctr .'"><div><h2>'.$sectionTitle.'</h2><p>'.$sectionDesc.'</p><a class="cta" href="'.get_permalink( $sectionURL).'"><span></span>Learn More</a></div></section>';
                                        $ctr++;
                                    }
                                } 
                            ?>
                            </div>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>