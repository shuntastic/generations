<?php
/**
 * Template Name: Landing Page Template Two
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" />
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/selectivizr.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>
</head>
<body <?php body_class(); ?>>
	<!-- Row for main content area -->

		<div class="row logoheader">
			<header id="nav-header" class="twelve columns required-header" role="banner">
	            <a class="header-image" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="/assets/cropped-header-logo.png" alt="" /></a>
			</header>
		</div><!-- // row header -->
	
<div id="content" class="row form">
	<div id="main" class="twelve columns nonav-page template-two" role="main">
	    <div id="mainvis"></div>
		<div class="post-box">
					<?php while ( have_posts() ) : the_post(); ?>
				        <div id="post-<?php the_ID(); ?>" class="entry-content">
				        	<h2><?php //the_title(); ?></h2>
				            <?php the_content(); ?>
				        </div><!-- .entry-content -->
					<?php endwhile; // end of the loop. ?>
	            <div class="formarea">
					<?php	
						$phonenum = simple_fields_value("phonenumber");
						if($phonenum) {
							echo '<div class="phonecallout">';
							echo $phonenum;
							echo ' </div>';
						}
					?>
					<?php	
						$formHeadline = simple_fields_value("formheadline");
						$formDesc = simple_fields_value("formdescription");
						$form = simple_fields_value("formurl");
						function curPageURL() {
							$pageURL = 'http';
							if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
							$pageURL .= "://";
							if ($_SERVER["SERVER_PORT"] != "80") {
								$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
							} else {
								$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
							}
							return $pageURL;
						}
						if($form){
							echo '<div class="formholder whitebox">';
							if($formHeadline) echo '<h2>'.$formHeadline.'</h2>';
							if($formDesc) echo '<p>'.$formDesc.'</p>';
							if($form) echo '<div style="margin:0px;padding:0px;overflow:hidden;height:350px;"><iframe src="'.$form.'?parenturl='.curPageURL().'" frameborder="0" style="overflow:hidden;height:150%;width:150%" height="150%" width="150%"></iframe></div>';
							echo '</div>';
						}
					?>
	            </div>
   					<?php	
                        $contentOutput = simple_fields_values('nonavhtml');
                        if ($contentOutput) {
							echo '<div class="contentarea">';
                        	$ctr = 1;
	                        foreach ($contentOutput as $value) {
	                            echo '<article class="sect'.$ctr.'">'.$value.'</article>';
								$ctr++;
							}
			                echo '</div>';
                        } 
                    ?>

					<?php	
                        $twocolOutput = simple_fields_values('leftcol,rightcol');
                        if ($twocolOutput) {
							echo '<div class="twocol">';
	                        foreach ($twocolOutput as $value) {
	                            echo '<div class="entry"><div class="leftcol">'.$value['leftcol'].'</div>';
	                            echo '<div class="rightcol">'.$value['rightcol'].'</div></div>';
								$ctr++;
							}
			                echo '</div>';
                        } 
                    ?>
            <?php 
            	$mapFlag = simple_fields_value('displaymap');
            ?>
	        <?php if ($mapFlag) : ?>
	        	<hr />
	            <div class="maparea whitebox">
		            <div class="map-controls">
		                <?php if ( is_active_sidebar( 'arbitrary' ) ) : ?>
		                <div class="map-widget">
		                    <?php dynamic_sidebar( 'arbitrary' ); ?>
		                </div><!-- #first .widget-area -->
		                <?php endif; ?>
		                <?php /*do_shortcode('[widget id="widget_tribe_widget_builder_137-3"]');*/ ?>
		                <div class="map-app"></div>
		            </div>
		            <div class="mapholder"><iframe src="<?php echo get_site_url(); ?>/locations-map-frame/" width="880" height="410"></iframe></div>
				</div>
            <?php endif; ?><!-- displaymap -->
		<?php	
            $disclaimerOutput = simple_fields_value('disclaimercopy');
			if ($disclaimerOutput) {
				echo '<div class="lpdisclaimer"><p class="disclaimer">'.$disclaimerOutput.'</div>';
			}
        ?>
       </div>
	</div>
            <?php get_footer(); ?>
	</div><!-- End Content row -->
</body>
</html>