<?php
/**
 * Template Name: Promotional Slider Template
 * Description: Template for slider in mid section
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" role="main">

			<div class="promo-holder">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php	
						$currID = 89;
                        $promos = simple_fields_get_post_group_values($currID, "Promo slider", false, 2); 
                        if ($promos) {
							echo '<div id="promoslide" class="flexslider"><ul class="slides">';
                            foreach ($promos as $value) {
								$image = wp_get_attachment_image_src($value[1], 'full');
                                echo '<li class="slide"><div class="imgHolder" style="background:transparent url('.$image[0].') center center no-repeat;background-size:cover;"></div><div class="copy"><h4>'.$value[2].'</h4><p>'.$value[3].'</p><a class="cta" href="'.get_permalink($value[5]).'"><span></span>'.$value[4].'</a></div></li>';
                            }
							echo '</ul></div>';
                        } 
                    ?>
					<?php //get_template_part( 'content', 'mid' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- /#main -->
	</div><!-- End Content row -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>