<?php
/**
 * Template Name: Tools Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/jquery.validate.min.js"></script>
</head>
<body <?php body_class(); ?> style="background:transparent;">
	<!-- Row for main content area -->
<!-- <script type="text/javascript"> function mktoGetForm() {return document.getElementById('mktForm_1004'); }</script> -->
<div id="content" class="row form">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
					<?php	
                        $contentOutput = simple_fields_values('nonavhtml');
                        if ($contentOutput) {
							echo '<div class="contentarea">';
                        	$ctr = 1;
	                        foreach ($contentOutput as $value) {
	                            echo '<article class="sect'.$ctr.'">'.$value.'</article>';
								$ctr++;
							}
			                echo '</div>';
                        } 
                    ?>
                        </div><!-- .entry-content -->
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>

	</div><!-- End Content row -->

<!-- GOOGLE ANALYTICS.  ENTER YOUR CODE and UNCOMMENT block -->
<!--
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-18664312-1";
urchinTracker();
</script>
-->
</body>
</html>