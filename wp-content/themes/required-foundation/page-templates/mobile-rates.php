<?php
/**
 * Template Name: Mobile Rates Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=.7, user-scalable=yes">
	<!-- <meta name="viewport" content="user-scalable=no,width=device-width" /> -->
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" />
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/selectivizr.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/mobile.css" />
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>
</head>
<body <?php body_class(); ?>>
	<!-- Row for main content area -->

		<div class="row logoheader">
			<header id="nav-header" class="six columns required-header" role="banner">
	            <a class="header-image" href="<?php echo esc_url( home_url( '/mobile' ) ); ?>"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/assets/cropped-header-logo.png" alt="" /></a>
				<nav id="access" class="orange-box" role="navigation">
					<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
	                        <?php dynamic_sidebar( 'sidebar-main' ); ?>
	                <?php endif; ?>
				</nav>
			</header>
		</div><!-- // row header -->
	
<div id="content" class="row form">
	<div id="main" class="twelve columns product-page rates mobile" role="main">
        	<?php
				$postNum = simple_fields_value("shortcode");
				$blogID = $postNum;	
				$file_info = simple_fields_value("mainvis",$blogID);
				$page_headline = simple_fields_value("headline",$blogID);
				$intro_copy = simple_fields_value("introcopy",$blogID);
				$rate_content = simple_fields_value("contentarea",$blogID);

    //             if ($file_info) {
    //                 echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
				// }
            ?>
            <div id="mainvis" class="fullgradient"></div>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                        </div><!-- .entry-content -->
						<?php if ( is_active_sidebar( 'arbitrary' ) ) : ?>
		                        <?php dynamic_sidebar( 'arbitrary' ); ?>
		                <?php endif; ?>
                        <div class="info">
                            <div id="main-content" class="nowidget">
                                <div class="section">
                                    <h2><?php the_title(); ?></h2>
                                    <?php echo $rate_content; ?>
                                    <?php //$post = get_page($blogID); echo $post->post_content; ?>
                                </div>
                            </div>                 
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
                </div>
            </div>
		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_sidebar(); ?>
<div class="returntodesktop"><a href="https://www.mygenfcu.org">Go To Desktop site</a></div>
<?php get_footer(); ?>
