<?php
/**
 * Template Name: Slideshow Template
 * Description: Template for slider in mid section
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/jquery.flexslider-min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/jquery-ui.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/jquery.validate.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/jquery.jscrollpane.min.js"></script>
</head>
<body <?php body_class(); ?>>
	<!-- Row for main content area -->
<div id="content" class="row">
		<div id="main" role="main">
			<div class="promo-holder">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php	
						$currID = $post->ID;
                        $promos = simple_fields_get_post_group_values($currID, "Image Slider assets", false, 2); 
                        if ($promos) {
							echo '<div id="slideshow" class="flexslider"><ul class="slides">';
                            foreach ($promos as $value) {
								$image = wp_get_attachment_image_src($value[1], 'full');
                                echo '<li class="slide"><div class="imgHolder" style="background:transparent url('.$image[0].') center center no-repeat;background-size:auto;"></div></li>';
                            }
							echo '</ul></div>';
                        } 
                    ?>
					<?php //get_template_part( 'content', 'mid' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- /#main -->
	</div><!-- End Content row -->
<script type="text/javascript">
	(function ($) {
	    	console.log('SLIDESHOW');
	    $(document).ready(function() {
	        if($('#slideshow.flexslider').length > 0) {
	            $('#slideshow.flexslider').flexslider({
	                slideshow: false,
	                directionNav: true
	            });
	        }
	    });
	}(jQuery));
</script>
<!-- GOOGLE ANALYTICS.  ENTER YOUR CODE and UNCOMMENT block -->
<!--
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-18664312-1";
urchinTracker();
</script>
-->
</body>
</html>