<?php
/**
 * Template Name: About the Blog Template
 * Description: The main template for the author page
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page about" role="main">
			<?php
				$blogID = 531;	
				$file_info = simple_fields_value("mainvis",$blogID);
				$page_headline = simple_fields_value("headline",$blogID);
				$intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <div class="row blognav">
								<?php wp_nav_menu( array(
                                    'menu' => 13,
                                    'container' => false,
                                    'menu_class' => 'inline-list left',
                                    'fallback_cb' => false
                                ) ); ?>
                            </div>
                        </div><!-- .entry-content -->
                        <div class="info">
                            <div id="main-content" class="nonav">
                                <div class="contributors">

                                <?php while ( have_posts() ) : the_post(); ?>
                                    <h2><?php the_title(); ?></h2>
                                    <p><?php the_content(); ?></p>
                                <?php endwhile; // end of the loop. ?>
                            </div>
                            </div>                 
							<?php if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>