<?php
/**
 * Template Name: Blog Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.1.0
 */
get_header(); ?>

    <!-- Row for main content area -->
    <div id="content" class="row fix">
        <div id="main" class="twelve columns info-page" role="main">
            <?php   
                $blogID = 531;  
                $file_info = simple_fields_value("mainvis",$blogID);
                $page_headline = simple_fields_value("headline",$blogID);
                $intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
                    echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
                }
            ?>
            <div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                            <h2><?php echo $page_headline; ?></h2>
                        </div><!-- .entry-content -->
                        <div class="row blognav">
                        <?php wp_nav_menu( array(
                            'menu' => 13,
                            'container' => false,
                            'menu_class' => 'inline-list left',
                            'fallback_cb' => false
                        ) ); ?>
                        </div>
                        <div class="info">
                            <ul class="secondary-left-sidebar">
                                <?php wp_list_categories('include=12,10,9,11&title_li='); ?> 
                            </ul>
                            <div id="main-content">
                                <?php //new query to limit number of posts
                                    $wp_query = new WP_Query(array( 'paged' => get_query_var( 'paged' ) ));
//                                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                    $wp_query->query($query_string."&cat=12,10,9,11&posts_per_page=5&paged=".$paged);
                                ?>
                                <?php // while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                <?php while ( have_posts() ) : the_post(); ?>
                                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                                                <div class="section">
                                                    <?php if ( has_post_thumbnail() ) {
                                                            the_post_thumbnail();
                                                        } 
                                                    ?>
                                                    <div class="copy <?php if ( !has_post_thumbnail() ) echo 'nothumb'; ?>">
                                                        <h2><?php the_title(); ?></h2>
                                                        <?php if (!is_author('genadm')) {
                                                                echo '<p class="byline">by '.the_author().'</p>';
                                                            } 
                                                         ?>
                                                         <div class="showhide">
                                                            <div class="show excerpt"><?php the_excerpt(); ?></div>
                                                            <div class="hide fullcontent"><?php the_content(); ?><a class="read-less" href="javascript:void(0);">READ LESS</a></div>
                                                         </div>

                                                     </div>
                                                </div>
                                            </div>                 
                                            <?php endwhile; // end of the loop. ?>
                                <div class="page-link">
                                <?php
                                    $big = 999999999; // need an unlikely integer
                                    echo paginate_links( array(
                                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                        'format' => '?paged=%#%',
                                        'current' => max( 1, get_query_var('paged') ),
                                        'total' => $wp_query->max_num_pages,
                                        'before' => '<span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>',
                                        'after' => '</div>'
                                    ) );
                                ?>
                            </div>
                            </div>                 
                            <?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-blog' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>

                    </div>
                </div>
            <?php get_footer(); ?>

        </div><!-- /#main -->
    </div><!-- End Content row -->
