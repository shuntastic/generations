<?php
/**
 * Template Name: Rates Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns product-page rates" role="main">
			<?php	
				$file_info = simple_fields_value("mainvis");
				$page_headline = simple_fields_value("headline");
				$intro_copy = simple_fields_value("introcopy");
				$rate_content = simple_fields_value("contentarea");

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
				}
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div class="breadcrumbs">
                            <?php 
                                    if(function_exists('bcn_display')){
                                        bcn_display();
                                    }
                            ?>
                        </div>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                            <?php //the_content(); ?>
                            <?php //edit_post_link(); ?> 
                        </div><!-- .entry-content -->
                        <div class="info">
							<?php	
                                    global $post; // Setup the global variable $post
								//	$my_wp_query = new WP_Query();
								//	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
								//	$pageID = $post->ID;
								//	$childExist = get_page_children( $pageID, $all_wp_pages);
								//	if($childExist) {
											$kiddies = wp_list_pages("title_li=&include=".$post->post_parent."&echo=0"); 
										if ( is_page() && $post->post_parent ) // Make sure we are on a page and that the page is a parent
											$kiddies .= wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");  
										else
											$kiddies .= wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");  
										
										if ( $kiddies ) {
											echo '<ul class="secondary-left-sidebar">';
												echo $kiddies;
											echo '</ul><div id="main-content" class="wmenubar"><div class="single-tab">';
										} else {
											echo '<div id="main-content"><div class="single-tab">';
										} 
		                                if ($rate_content) {
											echo $rate_content;
		                                }
                            ?>
	                        </div>
                            </div>
							<?php if ( is_active_sidebar( 'sidebar-secondary-1' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-secondary-1' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>