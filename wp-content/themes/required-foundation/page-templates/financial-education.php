<?php
/**
 * Template Name: Financial Education Landing Template
 * Description: This page displays the calendar and Financial Education landing
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns financialeducation-landing-page" role="main">
			<?php	
				$file_info = simple_fields_value("mainvis");
				$page_headline = simple_fields_value("headline");
				$intro_copy = simple_fields_value("introcopy");
                if ($file_info) {

                    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                        // if IE<=8
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';
                    } else {
                        // if IE>8
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"></div>';
                    }
                }

    //             if ($file_info) {
    //                 echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';
				// }
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                        </div><!-- .entry-content -->
                        <div id="finedcalendar">
                            <div class="whitebox">
                                <div class="evtholder">
                                    <h2>Classes Available</h2>
                                    <div id="eventlist" class="events"></div>
                                </div>
                                <div class="calholder">
                                    <div id="calendar"></div>
                                    <div class="eventlegend">
                                        <div class="personal"><span></span>Personal</div>
                                        <div class="business"><span></span>Business</div>
                                        <div class="student"><span></span>Student</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="info">
							<?php	
								$landingSections = simple_fields_values('sectiontitle,sectiondescription,sectionurl,videothumb1,videourl1,videohead1,videodesc1,cta1,videothumb2,videourl2,videoheadline2,videodesc2,cta2');
								//foreach ($tabOutput as $values) {
								//	echo "image has id: " . $values["slug_image"] . " and description " . $values["slug_description"];
								//}

                               // $ = simple_fields_get_post_group_values($currID, "Page Tabs", false, 2); 
                                if ($landingSections) {
                                    $ctr = 1;
                                  //   if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                                  //       $coverStyles =  'behavior: url(/backgroundsize.htc);';
                                  //   } else {
                                  //       $coverStyles = '';
                                  //   }
                                  // //  '.$ie.'
                                    foreach ($landingSections as $value) {
                                        $sectionTitle = $value['sectiontitle'];
                                        $sectionDesc = $value['sectiondescription'];
                                         $sectionURL = $value['sectionurl'];

                                         $videothumb1 = $value['videothumb1'];
                                         $videourl1 = $value['videourl1'];
                                         $videohead1 = $value['videohead1'];
                                         $videodesc1 = $value['videodesc1'];
                                         $cta1 = $value['cta1'];

                                         $videothumb2 = $value['videothumb2'];
                                         $videourl2 = $value['videourl2'];
                                         $videohead2 = $value['videoheadline2'];
                                         $videodesc2 = $value['videodesc2'];
                                         $cta2 = $value['cta2'];

										echo '<section class="sect'. $ctr .'"><div><h2><a href="'.get_permalink( $sectionURL).'">'.$sectionTitle.'</a></h2><p>'.$sectionDesc.'</p><a class="cta" href="'.get_permalink( $sectionURL).'">Learn More</a>';
                                        if($videourl1 != ''){
                                            $image_info1 = wp_get_attachment_image_src($videothumb1, "full");

                                            if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                                                // if IE<=8
                                               echo '<div class="vidHolder"><div class="video" style="background:transparent url('.$image_info1[0].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"><a href="'.$videourl1.'" class="colorbox-link"><span></span></a></div><div class="copy"><h2>'.$videohead1.'</h2><p>'.$videodesc1.'</p><a href="'.$videourl1.'" class="colorbox-link">'.$cta1.'</a></div></div>';

                                            } else {
                                                // if IE>8
                                                echo '<div class="vidHolder"><div class="video" style="background:transparent url('.$image_info1[0].') top left no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"><a href="'.$videourl1.'" class="colorbox-link"><span></span></a></div><div class="copy"><h2>'.$videohead1.'</h2><p>'.$videodesc1.'</p><a href="'.$videourl1.'" class="colorbox-link">'.$cta1.'</a></div></div>';
                                            }

                                        }
                                        if($videourl2 != ''){
                                             $image_info2 = wp_get_attachment_image_src($videothumb2, "full");
                                            if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                                                // if IE<=8
                                               echo '<div class="vidHolder"><div class="video" style="background:transparent url('.$image_info2[0].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"><a href="'.$videourl2.'" class="colorbox-link"><span></span></a></div><div class="copy"><h2>'.$videohead2.'</h2><p>'.$videodesc2.'</p><a href="'.$videourl2.'" class="colorbox-link">'.$cta2.'</a></div></div>';

                                            } else {
                                                // if IE>8
                                                echo '<div class="vidHolder"><div class="video" style="background:transparent url('.$image_info2[0].') top left no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"><a href="'.$videourl2.'" class="colorbox-link"><span></span></a></div><div class="copy"><h2>'.$videohead2.'</h2><p>'.$videodesc2.'</p><a href="'.$videourl2.'" class="colorbox-link">'.$cta2.'</a></div></div>';
                                            }
                                         //  echo '<div class="vidHolder"><div class="video" style="background:transparent url('.$image_info2[0].') top left no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"><a href="'.$videourl2.'" class="colorbox-link"><span></span></a></div><div class="copy"><h2>'.$videohead2.'</h2><p>'.$videodesc2.'</p><a href="'.$videourl2.'" class="colorbox-link">'.$cta2.'</a></div></div>';
                                        }
                                        echo '</div></section>';
                                        $ctr++;
                                    }
                                } 
                            ?>
                            </div>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>