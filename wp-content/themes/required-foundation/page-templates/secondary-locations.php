<?php
/**
 * Template Name: Secondary Locations Page Template
 * Description: The main template for the homepage. Based off of Info no widget
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

/*$var = $_GET["name"]);
if (isset($var)) {
    echo "This var is set so I will print.";
}
*/
get_header(); ?>
    <!-- Row for main content area -->
    <div id="content" class="row fix">
        <div id="main" class="twelve columns info-page" role="main">
            <?php   
                $file_info = simple_fields_value("mainvis");
                $page_headline = simple_fields_value("headline");
                $intro_copy = simple_fields_value("introcopy");

                if ($file_info) {
                    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                        // if IE<=8
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';
                    } else {
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
                    }

                }
            ?>
            <div class="post-box">
                <?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                            <h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                        </div><!-- .entry-content -->
                        <div class="info">
                            <?php wp_nav_menu( array(
                                'theme_location' => 'meta',
                                'container' => 'ul',
                                'menu_class' => 'secondary-left-sidebar',
                                'fallback_cb' => false
                            ) ); ?>
                            <div id="main-content" class="nowidget">
                                <div class="section">
                                    <h2><?php the_title(); ?></h2>
                                    <?php the_content(); ?>
                                    <div class="map-controls">
                                        <?php if ( is_active_sidebar( 'arbitrary' ) ) : ?>
                                        <div class="map-widget">
                                            <?php dynamic_sidebar( 'arbitrary' ); ?>
                                        </div><!-- #first .widget-area -->
                                        <?php endif; ?>
                                        <?php /*do_shortcode('[widget id="widget_tribe_widget_builder_137-3"]');*/ ?>
                                        <div class="map-app"><a href="http://locator.allpointnetwork.com/AllpointMobile.aspx" target="_blank">Download the locator App</a></div>
                                    </div>
                                    <div class="mapholder">
                                        <?php 
                                            $showlocs = array($_GET['showmap']);
                                          //  $width = isset($_GET['mapwidth'])) ? $_GET['mapwidth'] : '730';
                                          //  $height = isset($_GET['mapheight'])) ? $_GET['mapheight'] : '390';
                                     //       $width = $_GET['mapwidth']);
                                      //      $height = $_GET['mapheight'];
                                    //        echo '<script type="text/javascript"> console.log("MAP LOADED")</script>';
                                            $loclist = implode(',', $showlocs);
                                            if($loclist != '') {
                                                echo do_shortcode('[mashup query="posts_per_page=-1&post__in='.$loclist.'" iwType="iw" name="defaultmap" width="100%" height="390" initialopeninfo="false" directions="true"]');
                                            } else {
                                                echo do_shortcode('[mappress mapid="1" name="defaultmap" iwType="iw" width="100%" height="390" initialopeninfo="false" directions="false"]');
                                            }

                                        ?>
                                    </div>
<!--                                      <div class="mapholder"><?php // echo do_shortcode('[mappress mapid="1" name="defaultmap" width="729" height="400" initialopeninfo="false" directions="false"]'); ?></div>
 -->                               </div>
                            </div>                 
                   <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php endwhile; // end of the loop. ?>
                </div>
            </div>
        </div><!-- /#main -->
    </div><!-- End Content row -->
    
    <?php get_footer(); ?>
