<?php
/**
 * Template Name: Blog Videos Template
 * Description: The main template for the video page in the blogs section
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page about" role="main">
			<?php
				$blogID = 531;	
				$file_info = simple_fields_value("mainvis",$blogID);
				$page_headline = simple_fields_value("headline",$blogID);
				$intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <div class="row blognav">
								<?php wp_nav_menu( array(
                                    'menu' => 13,
                                    'container' => false,
                                    'menu_class' => 'inline-list left',
                                    'fallback_cb' => false
                                ) ); ?>
                            </div>
                        </div><!-- .entry-content -->
                        <div class="info">
                            <div id="main-content" class="nonav">
                                <div class="videoentries">

                                <?php while ( have_posts() ) : the_post(); ?>
                                <?php
                                $videoEntries = simple_fields_values('videothumb,videourl,videotitle,videodescription');
                                if ($videoEntries) {
                                    $ctr = 1;
                                    foreach ($videoEntries as $value) {
                                         $videothumb = $value['videothumb'];
                                         $videourl = $value['videourl'];
                                         $videohead = $value['videotitle'];
                                         $videodesc = $value['videodescription'];
                                        if($videourl != ''){
                                            $image_info = wp_get_attachment_image_src($videothumb, "full");
                                            echo '<section class="sect'. $ctr .'"><div class="vidHolder"><div class="video" style="background:transparent url('.$image_info[0].') top left no-repeat;background-size:cover;"><a href="'.$videourl.'" class="colorbox-link"><span></span></a></div><div class="copy"><h2>'.$videohead.'</h2><p>'.$videodesc.'</p><a href="'.$videourl.'" class="colorbox-link">WATCH VIDEO</a></div></div></section>';
                                        }
                                        $ctr++;
                                    }
                                } 
                                ?>
                                <?php endwhile; // end of the loop. ?>
                            </div>
                            </div>                 
							<?php if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>