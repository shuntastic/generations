<?php
/**
 * Template Name: Secondary Product Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns product-page" role="main">
			<?php	
				$file_info = simple_fields_value("mainvis");
				$page_headline = simple_fields_value("headline");
				$intro_copy = simple_fields_value("introcopy");

                if ($file_info) {
                    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                        // if IE<=8, add behavior: url(/backgroundsize.htc);
						echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';
                    } else {
						echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
                    }

                }
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div class="breadcrumbs">
                            <?php 
                                    if(function_exists('bcn_display')){
                                        bcn_display();
                                    }
                            ?>
                        </div>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                            <?php //the_content(); ?>
                            <?php //edit_post_link(); ?> 
                        </div><!-- .entry-content -->
                        <div class="info">
							<?php	
         //                            global $post; // Setup the global variable $post
									// $my_wp_query = new WP_Query();
									// $all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
									// $pageID = $post->ID;
									// $childExist = get_page_children( $pageID, $all_wp_pages);
									// $isParent = ( is_page() && $post->post_parent ); // Make sure we are on a page and that the page is a parent
									// $currentDepth = count(get_post_ancestors($pageID));

									// switch ($currentDepth) {
									//     case 1:
									//         echo '1';
									// 		$kiddies = wp_list_pages("title_li=&include=".$post->post_parent."&echo=0");
									// 		$kiddies .= wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0"); 
									// 		echo '<ul class="secondary-left-sidebar '.$currentDepth.'">';
									// 		echo $kiddies;
									// 		echo '</ul><div id="tabs" class="wmenubar">';
									//         break;
									//     case 2:
									//         echo "2";
   						// 					$kiddies = wp_list_pages("title_li=&include=".$post->post_parent."&echo=0");
									//         $kiddies .= wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");  
									// 		echo '<ul class="secondary-left-sidebar '.$currentDepth.'">';
									// 		echo $kiddies;
									// 		echo '</ul><div id="tabs" class="wmenubar">';
									//         break;
									//     case 3:
									//         echo "3";
									//         $kiddies = wp_list_pages("title_li=&include=".$post->post_parent."&echo=0");  
									// 		$kiddies .= '<ul class="children">';
									// 		$kiddies .= wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
									// 		$kiddies .= '</ul>';
									// 		echo '<ul class="secondary-left-sidebar '.$currentDepth.'">';
									// 		echo $kiddies;
									// 		echo '</ul><div id="tabs" class="wmenubar">';
 
									//         break;
									// }
                            ?>
								<?php 
									function wpe_highest_ancestor(){
									    global $post;

									    $ancestors = array($post->ancestors);
									    $page_ancestors = end( $ancestors );
									    if ( !empty($page_ancestors) ) {
									        $child_of = $page_ancestors;
									    } else {
									        $child_of = $post->ID;
									    }

									    return $child_of;
									}

								global $post;
								$pageID = $post->ID;
								$currentDepth = count(get_post_ancestors($pageID));
								$parent = $post->post_parent;
							  if($post->post_parent) {
								switch ($currentDepth) {
								    case 1:
								    //    echo '<p>1</p>';
										$children = wp_list_pages("title_li=&include=".$pageID."&echo=0");
										//$children .= wp_list_pages("title_li=&include=".$parent."&echo=0");
										$children .= wp_list_pages("title_li=&child_of=".$pageID."&echo=0");
								        break;
								    case 2:
								     //   echo '<p>2</p>';
										$children = wp_list_pages("title_li=&include=".$parent."&echo=0");
										$children .= wp_list_pages("title_li=&child_of=".$parent."&echo=0");
								        break;
								    case 3:
								     //   echo '<p>3</p>';


										$ancestors = get_post_ancestors($post);
										$children .= wp_list_pages("title_li=&include=".$parent."&echo=0");
										$children .= wp_list_pages("title_li=&child_of=".$parent."&echo=0");

										// $children .= wp_list_pages('title_li=&parent='.get_post($post->post_parent)->post_parent.'&echo=0');
										// $children .= wp_list_pages("title_li=&child_of=".$parent."&echo=0");

								        break;
								}
							   } else {
									  $children .= wp_list_pages("title_li=&child_of=".$pageID."&echo=0");
									
								}
							  if ($children) { ?>
							  <ul class="secondary-left-sidebar">
							  <?php echo $children; ?>
							  </ul>
							  <?php } ?>
                            <div id="tabs" class="wmenubar">               
							<?php	
                                $currID = $post->ID;
								
								$tabOutput = simple_fields_values('tablabel,tabcontent,contentlink,tabtype');
								//foreach ($tabOutput as $values) {
								//	echo "image has id: " . $values["slug_image"] . " and description " . $values["slug_description"];
								//}

                               // $ = simple_fields_get_post_group_values($currID, "Page Tabs", false, 2); 
                                if ($tabOutput) {
                                    $ctr = 1;
                                    echo '<ul>';
                                    foreach ($tabOutput as $value) {
                                        $selected_value = $value['tablabel'];
                                        echo '<li><a href="#tabs-'. $ctr .'">'.$selected_value.'</a></li>';
                                        $ctr++;
                                    }
                                    echo '</ul>';

                                    $ctr = 1;
                                    foreach ($tabOutput as $value) {
                                        $tab_content = $value['tabcontent'];
                                        $tab_link = $value['contentlink'];
                                        $tab_type = $value['tabtype'];
                                        if ($tab_content) {
											echo '<div class="tabcontent" id="tabs-'. $ctr .'">'.$tab_content.'</div>';
										} else if ($tab_link) {
											echo '<div class="tabcontent" id="tabs-'. $ctr .'">'.$tab_link.'</div>'; 
										} else if ($tab_type == 'radiobutton_num_4' ) {
											echo '<div class="tabcontent" id="tabs-'. $ctr .'"><div id="BVRRContainer"></div></div>'; 
										} else if ($tab_type == 'radiobutton_num_6' ) {
											echo '<div class="tabcontent" id="tabs-'. $ctr .'">';
												$faqOutput = simple_fields_values("question,answer"); 
												$faqDisclaimer = simple_fields_values("disclaimer");
												//$faqOutput = simple_fields_get_post_group_values($currID, "FAQs", false, 2); 
												if($faqOutput) {
													$subctr = 1;
													foreach ($faqOutput as $inf) {
														 echo '<pre class="faq"><a class="show_hide" href="#" rel="#slidingDiv_'.$subctr.'">'.$inf["question"].'</a></pre>';
														 echo '<div id="slidingDiv_'.$subctr.'" class="toggleDiv" style="display: none;">'.$inf["answer"].'</div>';
														 $subctr++;
													}
												}
											if ($faqDisclaimer) {
												echo '<div class="faqdisclaimer"><p>'.$faqDisclaimer.'</div>';
											}
											echo '</div>'; 
										}
                                        $ctr++;
                                    }
                                } 
                            ?>
                            </div>
							<?php if ( is_active_sidebar( 'sidebar-secondary-1' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-secondary-1' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>