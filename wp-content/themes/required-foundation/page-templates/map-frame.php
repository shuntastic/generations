<?php
/**
 * Template Name: Map Frame Template
 * Description: The main template for the homepage. Based off of Info no widget
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="robots" content="noindex, nofollow">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
?>

<script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>
</head>
<body <?php body_class(); ?> style="background:transparent;">
    <?php 
        $showlocs = array($_GET['showmap']);
      //  $width = isset($_GET['mapwidth'])) ? $_GET['mapwidth'] : '730';
      //  $height = isset($_GET['mapheight'])) ? $_GET['mapheight'] : '390';
 //       $width = $_GET['mapwidth']);
  //      $height = $_GET['mapheight'];
//        echo '<script type="text/javascript"> console.log("MAP LOADED")</script>';
        $loclist = implode(',', $showlocs);
        if($loclist != '') {
            echo do_shortcode('[mashup query="post__in='.$loclist.'" iwType="iw" name="defaultmap" width="100%" height="390" initialopeninfo="false" directions="false"]');
        } else {
            echo do_shortcode('[mappress mapid="1" name="defaultmap" iwType="iw" width="100%" height="390" initialopeninfo="false" directions="false"]');
        }

    ?>  <!-- <div class="mapholder"></div> -->

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
         chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7]>
        <script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
        <script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>

    <?php wp_footer(); ?>
 </body>
</html>
