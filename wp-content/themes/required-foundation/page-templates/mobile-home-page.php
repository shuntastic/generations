<?php
/**
 * Template Name: Mobile Home Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <!-- <meta name="viewport" content="width=device-width, initial-scale=.7, user-scalable=yes"> -->
	<!-- <meta name="viewport" content="user-scalable=yes,width=device-width" /> -->
	<meta id="siteport" name="viewport" content="width=device-width, initial-scale=.7, user-scalable=yes">
	<script>
		if (screen.width < 700) {
			var mvp = document.getElementById('siteport');
			mvp.setAttribute('content','width=380');
		}
	</script>
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" />
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/selectivizr.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/mobile.css" />
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>
</head>
<body <?php body_class(); ?>>
	<!-- Row for main content area -->

		<div class="row logoheader">
			<header id="nav-header" class="six columns required-header" role="banner">
	            <a class="header-image" href="<?php echo esc_url( home_url( '/mobile' ) ); ?>"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/assets/cropped-header-logo.png" alt="" /></a>
				<nav id="access" class="orange-box" role="navigation">
					<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
	                        <?php dynamic_sidebar( 'sidebar-main' ); ?>
	                <?php endif; ?>
				</nav>
			</header>
		</div><!-- // row header -->
	
<div id="content" class="row form">
	<div id="main" class="twelve columns mobile" role="main">
				<?php	
                    $currID = 5;
                    $hero = simple_fields_get_post_group_values($currID, "Homepage hero", false, 2); 
                    if ($hero) {
						echo '<div id="homeslide" class="flexslider"><ul class="slides">';
                        foreach ($hero as $value) {
							$image = wp_get_attachment_image_src($value[1], 'full');
							$selected_value = $value[7];
							$external = simple_fields_values("ctaexternal");
							$destURL = '';
							if($external) {
								$destURL = $external[0];
							} else {
								$destURL = get_permalink($value[4]);
							}
                            echo '<li class="slide"><img src="'.$image[0].'" /><div class="callHolder '.$selected_value.'"><div class="callout">'.$value[2].'<a href="'.$destURL.'">'.$value[3].'</a></div></div></li>';
                            // echo '<li class="slide" style="background:transparent url('.$image[0].') no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"><div class="callHolder '.$selected_value.'"><div class="callout">'.$value[2].'<br /><a href="'.$destURL.'">'.$value[3].'</a></div></div></li>';

                        }
						echo '</ul></div>';
                    } 
                ?>
		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_sidebar(); ?>
<div class="returntodesktop"><a href="https://www.mygenfcu.org">Go To Desktop site</a></div>
<?php get_footer(); ?>
