<?php
/**
 * Template Name: Resource Center Page
 * Description: This page displays the Resource Center Page in Business Banking
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns resourcecenter" role="main">
			<?php	
				$file_info = simple_fields_value("mainvis");
				$page_headline = simple_fields_value("headline");
				$intro_copy = simple_fields_value("introcopy");

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') center center no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
                        <div class="breadcrumbs">
                            <?php 
                                    if(function_exists('bcn_display')){
                                        bcn_display();
                                    }
                            ?>
                        </div>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                            <p><?php echo $intro_copy; ?></p>
                        </div><!-- .entry-content -->
                                    <?php   
                                        $startup = simple_fields_value("startup");
                                        $actioncenter = simple_fields_value("actioncenter");
                                        $benefitsglance = simple_fields_value("benefitsglance");
                                        $compareproducts = simple_fields_value("compareproducts");
                                        $alsointerest = simple_fields_value("alsointerest");
                                    ?>
                        <div class="info">
                           <div class="whitesemibox">
                                <div class="columnwrap">
                                    <div class="columnone">
                                        <div class="section startup">
                                            <?php if ($startup) echo $startup; ?>
                                        </div>
                                        <div class="section actioncenter">
                                            <?php if ($actioncenter) echo $actioncenter; ?>
                                        </div>
                                    </div>
                                </div>
                                    <div class="columntwo">
                                        <div class="section benefitsglance">
                                            <?php if ($benefitsglance) echo $benefitsglance; ?>
                                        </div>
                                        <div class="section compareproducts">
                                            <?php if ($compareproducts) echo $compareproducts; ?>
                                        </div>
                                        <div class="section alsointerest greybox">
                                            <?php if ($alsointerest) echo $alsointerest; ?>
                                        </div>
                                    </div>
                                <div class="disclaimer"><p>To be eligible for these services and products your business must be located in Bexar County, Texas.</p></div>
                            </div>
                        </div>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php endwhile; // end of the loop. ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>