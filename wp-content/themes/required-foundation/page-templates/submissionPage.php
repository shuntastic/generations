<?php
/**
 * Template Name: Submission Container Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>
<script type="text/javascript" src="//generationsfcu.ugc.bazaarvoice.com/bvstaging/static/4523-en_us/bvapi.js"></script>
<script type="text/javascript">
    $BV.configure("global", {
            userToken: "",
            doLogin: function(successCallback, successUrl) {
            window.location = "http://www.mygenfcu.org/login.html?return=" + encodeURIComponent(successUrl);
        }
    });
</script>
	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns info-page writeareview" role="main">
            <div id="mainvis"></div>
            <div class="post-box">
                <div class="info">
                    <div id="main-content" class="nonav">
                        <div class="contributors">

                        <?php while ( have_posts() ) : the_post(); ?>
                            <h2><?php the_title(); ?></h2>
                            <div id="BVSubmissionContainer"></div>
                        <?php endwhile; // end of the loop. ?>
                    </div>
                    </div>                 
                    <?php if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
                        <div class="secondary-right-sidebar widget-area">
                            <?php dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                        </div><!-- #first .widget-area -->
                    <?php endif; ?>
                   <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
                </div>
            </div>
		</div><!-- /#main -->
    </div><!-- End Content row -->
	<div id="secondary" class="widget-area" role="complementary"></div><!-- #secondary .widget-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>