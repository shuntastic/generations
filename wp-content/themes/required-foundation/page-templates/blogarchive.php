<?php
/**
 * Template Name: Blog Archive Page Template
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>
    <!-- Row for main content area -->
    <div id="content" class="row fix">
        <div id="main" class="twelve columns info-page" role="main">
            <?php
                $blogID = 531;  
                $file_info = simple_fields_value("mainvis",$blogID);
                $page_headline = simple_fields_value("headline",$blogID);
                $intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
                    echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
                }
            ?>
            <div class="post-box">
                <?php while ( have_posts() ) : the_post(); ?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                            <h2><?php echo $page_headline; ?></h2>
                            <div class="row blognav">
                                <?php wp_nav_menu( array(
                                    'menu' => 13,
                                    'container' => false,
                                    'menu_class' => 'inline-list left',
                                    'fallback_cb' => false
                                ) ); ?>
                            </div>
                        </div><!-- .entry-content -->
                        <div class="info">
                            <div id="main-content" class="nonav">
                                <div class="archives">

                                    <div class="one-col">
                                        <h2>By Month</h2>
                                        <ul>
                                            <?php wp_get_archives('type=monthly'); ?>
                                        </ul>
                                    </div>
                                    <div class="one-col catlist">
                                        <h2>By Category</h2>
                                        <ul>
                                             <?php wp_list_categories('title_li='); ?>
                                        </ul>
                                    </div>
                                    <div class="one-col">
                                        <h2>By Contributors</h2>
                                        <ul>
                                             <?php wp_list_authors('show_fullname=1&orderby=name&order=DESC'); ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>                 
                            <?php if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    
                                    <?php dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
                   <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php endwhile; // end of the loop. ?>
                    </div>
                </div>

        </div><!-- /#main -->
    </div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
