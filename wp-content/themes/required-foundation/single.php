<?php
/**
 * The template for displaying all single posts.
 *
 * This is the template that displays all single posts by default.
 * Please note that this is the WordPress construct of posts
 * and that other 'posts' on your WordPress site will use a
 * different template.
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.3.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page" role="main">
			<?php	
				$blogID = 531;	
				$file_info = simple_fields_value("mainvis",$blogID);
				$page_headline = simple_fields_value("headline",$blogID);
				$intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
					echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
				}
            ?>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                        </div><!-- .entry-content -->
                        <div class="row blognav">
						<?php wp_nav_menu( array(
							'menu' => 13,
							'container' => false,
							'menu_class' => 'inline-list left',
							'fallback_cb' => false
						) ); ?>
                        </div>
                        <div class="info">
							<ul class="secondary-left-sidebar">
								<?php wp_list_categories('include=12,10,9,11&title_li='); ?> 
                            </ul>
                            <div id="main-content">
								<?php //new query to limit number of posts
                                    $wp_query = new WP_Query();
                                    $wp_query->query($query_string."&posts_per_page=2&paged=".$paged);
                                ?>
								<?php while ( have_posts() ) : the_post(); ?>
                                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                                                <div class="section">
													<?php if ( has_post_thumbnail() ) {
                                                            the_post_thumbnail();
                                                        } 
                                                    ?>
                                                    <div class="copy">
                                                    	<h2><?php the_title(); ?></h2>
                                                 		 <p class="byline">by <?php the_author(); ?></p>
                                                            <div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php the_permalink(); ?>" addthis:title="<?php the_title(); ?>">
                                                                <a class="addthis_button_facebook"></a>
                                                                <a class="addthis_button_twitter"></a>
                                                                <a class="addthis_button_google_plusone_share"></a>
                                                                <a class="addthis_button_email"></a>
                                                                <a class="addthis_button_compact"></a>
                                                            </div>
                                                         <span class="date"><?php the_date(); ?> -&nbsp;</span><?php the_content(); ?>
                                                     </div>
                                                </div>
                                            </div>                 
                                <?php endwhile; // end of the loop. ?>
                            </div>                 
							<?php if ( is_active_sidebar( 'sidebar-secondary-2' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php dynamic_sidebar( 'sidebar-secondary-2' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
		            </div>
                </div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php //get_sidebar(); ?>
    <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51d353fb7fb5f49b"></script> 
<?php get_footer(); ?>

