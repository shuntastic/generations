<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.1.0
 */
get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row fix">
		<div id="main" class="twelve columns info-page" role="main">
			<?php	
				$blogID = 531;	
				$file_info = simple_fields_value("mainvis",$blogID);
				$page_headline = simple_fields_value("headline",$blogID);
				$intro_copy = simple_fields_value("introcopy",$blogID);

                if ($file_info) {
                    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                        // if IE<=8, add behavior: url(/backgroundsize.htc);
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';

                    } else {
                        echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$file_info["url"].'", sizingMethod="scale");"></div>';
                    }
                }
            ?>
			<div class="post-box">
                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                        	<h2><?php echo $page_headline; ?></h2>
                        </div><!-- .entry-content -->
                        <div class="row blognav">
						<?php wp_nav_menu( array(
							'menu' => 13,
							'container' => false,
							'menu_class' => 'inline-list left',
							'fallback_cb' => false
						) ); ?>
                        </div>
                        <div class="info">
							<ul class="secondary-left-sidebar">
								<?php wp_list_categories('include=12,10,9,11&exclude=8&title_li='); ?> 
                            </ul>
                            <div id="main-content">
								<?php //new query to limit number of posts
                                    $wp_query = new WP_Query(array( 'paged' => get_query_var( 'paged' ) ));
//                                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                    $wp_query->query($query_string."&cat=12,10,9,11&posts_per_page=5&paged=".$paged);
                                ?>
                                <?php //while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                <?php while ( have_posts() ) : the_post(); ?>
                                        <div id="post-<?php the_ID(); ?>" class="entry-content">
                                                <div class="section">
													<?php if ( has_post_thumbnail() ) {
                                                            the_post_thumbnail();
                                                        } 
                                                    ?>
                                                    <div class="copy <?php if ( !has_post_thumbnail() ) echo 'nothumb'; ?>">
                                                    	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                                        <?php 
                                                            $curAuthor = get_the_author();
                                                            if ($curAuthor != 'Generations Admin') :
                                                        ?> 
                                                            <p class="byline">by <?php the_author(); ?></p>
                                                        <?php endif; ?>
                                                         <div class="showhide">
                                                            <div class="show excerpt"><span class="date"><?php the_date(); ?> -&nbsp;</span><?php the_excerpt(); ?></div>
                                                            <div class="hide fullcontent">
                                                                <div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php the_permalink(); ?>" addthis:title="<?php the_title(); ?>">
                                                                <a class="addthis_button_facebook"></a>
                                                                <a class="addthis_button_twitter"></a>
                                                                <a class="addthis_button_google_plusone_share"></a>
                                                                <a class="addthis_button_email"></a>
                                                                <a class="addthis_button_compact"></a>
                                                                </div>
                                                                <?php the_content(); ?>
                                                                <a class="read-less" href="javascript:void(0);">READ LESS</a></div>
                                                         </div>

                                                     </div>
                                                </div>
                                            </div>                 
                                            <?php endwhile; // end of the loop. ?>
                                <div class="page-link">
                                <?php if ( function_exists( 'required_pagination' ) ) {
                                    required_pagination();
                                } ?>

                            </div>
                            </div>                 
                            <?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
                                <div class="secondary-right-sidebar widget-area">
                                    <?php echo do_shortcode('[widget id="widget_tribe_widget_builder_111-7"]'); ?>
                                    <?php dynamic_sidebar( 'sidebar-blog' ); ?>
                                </div><!-- #first .widget-area -->
                            <?php endif; ?>

		            </div>
                </div>
		</div><!-- /#main -->
	</div><!-- End Content row -->
            <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51d353fb7fb5f49b"></script> 
            <?php get_footer(); ?>
