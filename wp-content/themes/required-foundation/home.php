<?php
/**
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
		<div id="main" class="twelve columns" role="main">
			<div class="post-box">
				<?php while ( have_posts() ) : the_post(); ?>
						<?php if ( is_active_sidebar( 'sidebar-home-1' ) ) : ?>
                        <div class="slider-sidebar widget-area">
                            <?php dynamic_sidebar( 'sidebar-home-1' ); ?>
                        </div><!-- #first .widget-area -->
                        <?php endif; ?>
                    </div>
	
    				<?php // get_template_part( 'content', 'page' ); ?>
					<?php	
                        $currID = $post->ID;
                        $hero = simple_fields_get_post_group_values($currID, "Homepage hero", false, 2); 
                        if ($hero) {
							echo '<div id="homeslide" class="flexslider"><ul class="slides">';
                            foreach ($hero as $value) {
								$image = wp_get_attachment_image_src($value[1], 'full');
								$selected_value = $value[7];
								$external = simple_fields_values("ctaexternal");
								$destURL = '';
								if($external) {
									$destURL = $external[0];
								} else {
									$destURL = get_permalink($value[4]);
								}
                                echo '<li class="slide" style="background:transparent url('.$image[0].') center center no-repeat;background-size:cover;"><div class="calloutArea"><div class="callHolder '.$selected_value.'" style="left:'.$value[5].';top:'.$value[6].';"><div class="callout">'.$value[2].'</div><a class="cta" href="'.$destURL.'"><span></span>'.$value[3].'</a></div></div></li>';
                            }
							echo '</ul></div>';
                        } 
                    ?>
					<?php //get_template_part( 'content', 'mid' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- /#main -->
	</div><!-- End Content row -->
<?php get_sidebar(); ?>

<?php get_footer(); ?>