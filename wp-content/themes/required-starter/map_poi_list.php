<select>
	<option value="default"><?php _e('Select a location', 'mappress');?></option>
	<?php $ctr = 0; ?>
	<?php foreach($map->pois as $poi) : ?>
	  <option value="<?php echo $ctr; ?>" data-address="<?php echo $poi->get_address(); ?>">
	  	<div class="map-icon">
	  		<?php echo $poi->get_icon(); ?>
	  	</div>
	  	<div class="map-info">
			<div class='mapp-title'>
				<?php echo $poi->get_open_link(); ?>
			</div>
	  	</div>
	  </option>
	  <?php $ctr += 1; ?>
	<?php endforeach; ?>
</select>
<span></span>
<script>
  (function ($) {
      $(document).ready(function() {

          $('#defaultmap_poi_list select').on('change', function() {
                if ($('body.page-template-page-templatesmobile-locations-php').length > 0) {
                	if (this.value !='default') {
	                    var locInfo = defaultmap.getPoi(this.value);
	                    console.log('locInfo',locInfo);
	                    defaultmap.getPoi(this.value).open();
	                    $('.map-status').remove();
	                    $('#defaultmap_poi_list').after('<div class="map-status">'+locInfo.body+'</div>');
	                } else {
	                	$('.map-status').remove();
	                }
                } else {
	              defaultmap.getPoi(this.value).open();
	              return false;
                }
          });


      });
  }(jQuery));
</script>
