<?php
	$width = $map->width();
	$height = $map->height();
	
	$id = $map->name . '_layout';
	$style = "width: $width; " . $map->get_layout_style();
	$class = "mapp-layout mapp-align-" . $map->options->alignment;
?>

<?php echo $map->get_show_link(); ?>
<div id="<?php echo $map->name . '_poi_list';?>" class="mapp-poi-list" style="width:100%"></div>
<div id="<?php echo $id; ?>" class="<?php echo $class; ?>" style="<?php echo $style; ?>">
	<div id="<?php echo $map->name . '_links';?>" class="mapp-map-links"><?php echo $map->get_links(); ?></div>
	<div id="<?php echo $map->name . '_dialog';?>" class="mapp-dialog"></div>
	<div id="<?php echo $map->name . '_directions';?>" class="mapp-directions">
		<form action='#'>
			<div>
				<a href='#' class='mapp-travelmode mapp-travelmode-on' title='<?php esc_html_e('By car', 'mappress'); ?>'><span class='mapp-dir-icon mapp-dir-car'></span></a>
				<a href='#' class='mapp-travelmode' title='<?php esc_html_e('Public Transit', 'mappress'); ?>'><span class='mapp-dir-icon mapp-dir-transit'></span></a>
				<a href='#' class='mapp-travelmode' title='<?php esc_html_e('Walking', 'mappress'); ?>'><span class='mapp-dir-icon mapp-dir-walk'></span></a>
				<a href='#' class='mapp-travelmode' title='<?php esc_html_e('Bicycling', 'mappress'); ?>'><span class='mapp-dir-icon mapp-dir-bike'></span></a>
			</div>


			<div class='mapp-route'>
				<a href='#' class='mapp-myloc'><?php _e('My location', 'mappress'); ?></a>

				<div>
					<span class='mapp-dir-icon mapp-dir-a'></span>
					<input class='mapp-dir-saddr' tabindex='1'/>
					<a href='#' class='mapp-dir-swap'><span class='mapp-dir-icon mapp-dir-arrows' title='<?php _e ('Swap start and end', 'mappress'); ?>'></span></a>

				</div>
				<div class='mapp-dir-saddr-err'></div>

				<div>
					<span class='mapp-dir-icon mapp-dir-b'></span>
					<input class='mapp-dir-daddr' tabindex='2'/>
				</div>
				<div class='mapp-dir-daddr-err'></div>
			</div>

			<div style='margin-top: 10px;'>
				<input type='submit' class='mapp-dir-get' value='<?php esc_html_e('Get Directions', 'mappress'); ?>'/>
				<a href='#' class='mapp-dir-print'><?php _e('Print', 'mappress'); ?></a>
				&nbsp;<a href='#' class='mapp-dir-close'><?php _e('Close', 'mappress'); ?></a>
				<span class='mapp-spinner' style='display:none'></span>
			</div>
		</form>
		<div class='mapp-dir-renderer'></div>
	</div>
	<div id="<?php echo $map->name;?>" class="mapp-canvas" style="<?php echo "width: 100%; height: $height; "; ?>"></div>
</div>
