/**
 * Author: Coloring Book Studio (SAS)
 */
function geocodeLocation (results,status) {
    console.log('geocodeLocation',results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log('Map OK', results);
                    map.setCenter(results[0].geometry.location);
                    // var marker = new google.maps.Marker({
                    //      map: map,
                    //      position: results[0].geometry.location
                  //   });
                } else {
                   console.log('Geocode was not successful for the following reason: ' + status);
                }
}
var mapSearch = null;
(function ($) {
    var mapHandler = {
        map: null,
        mapSearch:null,
        mapFrame: null,
        init: function() {
            var map = google.maps.Map(document.getElementById('mapp0'));
/*            google.maps.event.addListener(map, 'click', function(kmlEvent) {
                console.log('mapClicked', kmlEvent);
                //mapHandler.showKmlWindow(kmlEvent);
            });
*/            
            google.maps.event.addListener(map, 'click', function(e) {
               // console.log('CLICKED',e);
            });
        },
        initEventMap: function() {
            //var map = google.maps.Map(document.getElementById('googlemaps');
             //   console.log('initEventMap',event_address);
            // google.maps.event.addListener(marker, 'click', function() {
            //     infowindow.open(map,marker);
            // });

        },
        initMapDD:function () {
              $('#defaultmap_poi_list select').each(function(index) {
                $(this).change(function(){
                  defaultmap.getPoi(this.value).open();
                  return false;
                });
              });

        },
        panMap: function(zip) {
            var mapVal = -1;
            
              $('#defaultmap_poi_list option').each(function(index,value) {
                    var curZip = ($(this).data('address') != undefined) ? $(this).data('address') : '';
                    if(curZip != '') {
                        if(curZip.indexOf(zip) > -1) {
                            mapVal = this.value;
                            return false
                        }
                    }
              });
              if (mapVal > -1)  {
                if ($('body.page-template-page-templatesmobile-locations-php').length > 0) {
                    var locInfo = defaultmap.getPoi(this.value);
                    defaultmap.getPoi(mapVal).open();
                    $('.map-status').remove();
                    $('#defaultmap_poi_list').after('<div class="map-status">'+locInfo.body+'</div>');
                } else {
                    defaultmap.getPoi(mapVal).open(12);
                    $('#findLoc .map-status').remove();
                }
              } else {
                $('#findLoc a.cta').after('<span class="map-status">&nbsp;&nbsp;Location Not Found</span>');
              }

        },
        initWidget:function() {
            $('form#findLoc input[name="zip"]').inputToggle();
            var xp = getParams();
            //console.log('initWidget',mapdata);
            if((xp['showmap'] || xp['zip']) && (($('body.page-template-page-templatessecondary-locations-php').length > 0) || ($('body.page-template-page-templatesmobile-locations-php').length > 0))) {
                if(xp['showmap']) {
                    var mapVals = xp['showmap'].split(',');
                    $('form#findLoc input[type="checkbox"]').each(function(index,value){
                        console.log('locations', $(this).val());
                        if($.inArray($(this).val(), mapVals) >= 0) {
                            $(this).attr('checked',true);
                        } else {
                            $(this).attr('checked',false);
                        }
                    });
                }
                if(xp['zip']) {
                   // mapSearch = xp['zipcode'];
                    mapHandler.panMap(xp['zip']);
                    //if($('#mapFrame').length > 0) {
                        //var mapFrame = document.getElementById('mapFrame');
                        //mapFrame.contentWindow.panMap(xp['zipcode']);
                    //}
                }

                // var newFrame = ($('#main.nonav-page .mapholder').length>0) ? '<iframe src="'+generations.basePath+'/locations-map-frame/?showmap='+xp['showmap']+'" width="880" height="450" id="mapFrame">': '<iframe src="'+generations.basePath+'/locations-map-frame/?showmap='+xp['showmap']+'" width="730" height="450" id="mapFrame"></iframe>';
                // $('.mapholder').html(newFrame);
                
                //var address = ($('form#findLoc input[name="zip"]').val() != 'ZIPCODE') ? ('&zipcode='+$('form#findLoc input[name="zip"]').val()) : '';
                //window.location = generations.basePath+'/locations/?showmap='+xp['showmap']+address;


            }
            //var mapFrame = document.getElementById('mapFrame');
            // document.getElementById('mapFrame').contentWindow.targetFunction();
             $('form#findLoc input[type="checkbox"]').click(function() {
                if ($('.mapholder').length>0) {
                    var x=[];
                    $('form#findLoc input[type="checkbox"]:checked').each(function(){
                        x.push($(this).val());
                    });
                    // if (x==[]) {
                    //     $('form#findLoc input[name="location"]').attr('checked','checked');
                    // }

                    // var newFrame = ($('#main.nonav-page .mapholder').length>0) ? '<iframe src="'+generations.basePath+'/locations-map-frame/?showmap='+x.join(',')+'" width="880" height="450" id="mapFrame">': '<iframe src="'+generations.basePath+'/locations-map-frame/?showmap='+x.join(',')+'" width="730" height="450" id="mapFrame"></iframe>';
                    // console.log('checking',x.join(','), newFrame);
                    // $('.mapholder').html(newFrame);
                    window.location = ($('#main.mobile').length > 0) ? (generations.basePath+'/mobile/locations/?showmap='+x.join(',')) : (generations.basePath+'/locations/?showmap='+x.join(','));

                }/* else {

                }*/
             });
             $('form#findLoc a.cta').click(function(e) {
                if ($('body.page-template-page-templatessecondary-locations-php').length > 0) {
                    var address = ($('form#findLoc input[name="zip"]').val() != 'ZIPCODE') ? $('form#findLoc input[name="zip"]').val() : '';
                   if(address!='') mapHandler.panMap(address);

                } else {
                    var x=[];
                    $('form#findLoc input[type="checkbox"]:checked').each(function(){
                        x.push($(this).val());
                    });
                    if (x==[]) {
                        $('form#findLoc input[name="location"]').attr('checked','checked');
                    }
                    //x.join(',');
                    
                    //
                    var address = ($('form#findLoc input[name="zip"]').val() != 'ZIPCODE') ? ('&zip='+$('form#findLoc input[name="zip"]').val()) : '';

                    window.location = ($('#main.mobile').length > 0) ? (generations.basePath+'/mobile/locations/?showmap='+x.join(',')+address) : (generations.basePath+'/locations/?showmap='+x.join(',')+address);

                }
                e.preventDefault();
            });
        },
        showKmlWindow: function(e, layer) {
            if (infowindow) {
                infowindow.close();
            }

            //var windowContent = '<div><div class="info_window"><p>' + e.featureData.description+'</p></div><div class="windowTail"></div></div>';
            var windowContent = '<div><div class="info_window"><h3>'+e.featureData.name+'</h3><p>' + e.featureData.description+'</p></div><div class="windowTail"></div></div>';
            var infOptions = {
                    /*content: windowContent,*/
                    disableAutoPan: true,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(-100,-180),
                    zIndex: 0,
                    closeBoxMargin: "0px 0px -20px 0px",
                    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                    infoBoxClearance: new google.maps.Size(0, 0),
                    isHidden: false,
                    pane: "floatPane",
                    alignBottom:false,
                    enableEventPropagation: false
            };
            if(e.featureData.description != '') {
                infowindow = new InfoBox(infOptions);
                infowindow.setContent(windowContent);
                infowindow.setPosition(e.latLng);
                GoogleMap.map.panTo(e.latLng);
                infowindow.open(GoogleMap.map); 
            }

        //  if (infowindow) {
        //      infowindow.close();
        //  }
        //  infowindow = new google.maps.InfoWindow();
        //  infowindow.setPosition(e.latLng);
        //    infowindow.open(GoogleMap.map);
        //    infowindow.setContent('<div class="info_window"><h3>' + e.featureData.name + '</h3>' + e.featureData.description+'</div>');
    }       
    };
    var evtHandler = {
        calData: [],
        feedURL: '/events/rss',
      //  feedURL: 'generations/events/rss',
        //feedURL: (window.location.origin==v') ? (window.location.origin+'/generations/events/rss'):(window.location.origin+'/events/rss'),

        init: function() {
            //console.log('evtHandler INIT');
                $.get(generations.basePath+evtHandler.feedURL, function(feed) {
                    var today = new Date();
                    var $xml = $(feed);
                    console.log('feed',$xml.find("item").length);
                    $xml.find("item").each(function() {
                        var $this = $(this);
                        var item = {
                                title: $this.find("title").text(),
                                link: $this.find("link").text(),
                                description: $this.find("description").text(),
                                date: $this.find("date").text(),
                                category: $this.find("category").text()
                        }
                        evtDate = item.date.split('|');
                        item.startdate = evtDate[0];
                        item.enddate = evtDate[1];
                        var dateTrim = evtDate[1];
                        if(dateTrim) {
                            //dateTrim = dateTrim.trim().replace(' ','');
                            item.date = new Date(dateTrim);
                        }
                        var curCat = (item.category).split(':')
                        var catTrim = curCat[1];
                        if(catTrim) {
                            catTrim = catTrim.trim().replace(' ','');
                            item.category = catTrim.toLowerCase();
                        }
                     // console.log('item',item);
                        evtHandler.calData.push(item);
                    });
                    evtHandler.loadCalendar(today);
                }).fail(function() {
                   // $('#secondary #widget_tribe_widget_builder_727-2').css('display','none');
                    $('#eventlist').append('<p>There are no classes currently scheduled.</p>');
                    var today = new Date();
                    $('#calendar').datepicker({
                        dayNamesMin: ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                        buttonImageOnly:true,
                        minDate: today
                    });
                });

        },
        loadCalendar: function(today){
                  // console.log('loadCalendar');
                   if($('#secondary #widget_tribe_widget_builder_727-2').length>0) {
                        evtHandler.showEvents(evtHandler.calData,3);
                        $('.widget_tribe_widget_builder_727 #calendar').datepicker({
                            dayNamesMin: ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                            buttonImageOnly:true,
                            minDate: today,
                            beforeShowDay:evtHandler.beforeEventsDay,
                            onChangeMonthYear: evtHandler.onChangeDate,
                            onSelect: function(e) {
                                evtHandler.onSelect(e);
                                // console.log('calendar click',e);
                            }
                        });
                    }
                    if($('#finedcalendar').length>0) {
                        evtHandler.showEvents(evtHandler.calData,0);
                        //evtHandler.showEventsLP(calItems,0);
                        $('#finedcalendar #calendar').datepicker({
                            dayNamesMin: ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                            buttonImageOnly:true,
                            minDate: today,
                            beforeShowDay:evtHandler.beforeEventsDay,
                            onChangeMonthYear: evtHandler.onChangeDate,
                            onSelect: function(e) {
                                evtHandler.onSelect(e);
                            }
                        });
                    }
                    if($('#eventscalendar').length>0) {
                        $('#eventscalendar #calendar').datepicker({
                            dayNamesMin: ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                            buttonImageOnly:true,
                            minDate: today,
                            beforeShowDay:evtHandler.beforeEventsDay,
                            onChangeMonthYear: evtHandler.onChangeDate,
                            onSelect: function(e) {
                                evtHandler.onSelect(e);
                            }
                        });
                    }
        },
        showEvents: function(calItems,limit) {
                  $('#eventlist').html('');
                  $.each(calItems,function(index,value) {
 //                 var evtDate = new Date(this.enddate);
//                  this.date = evtDate;
                    var startTime = new Date(this.startdate);
                    var startHours = (parseInt(startTime.getHours()) > 12) ? parseInt(startTime.getHours()) - 12 : startTime.getHours();
                    var startMinutes = (startTime.getMinutes() == '0') ? '00' : startTime.getMinutes();

                    var endTime = new Date(this.enddate);
                    var endHours = (parseInt(endTime.getHours()) > 12) ? parseInt(endTime.getHours()) - 12 : endTime.getHours();
                    var endMinutes = (endTime.getMinutes() == '0') ? '00' : endTime.getMinutes();
                    var ampm = (parseInt(endTime.getHours()) > 12) ? 'pm' : 'am';


                    //'<p class="event" data-month="'+(startTime.getMonth()+1)+'"><span class="green">'+this.startdate+' - '+endHours+':'+endMinutes+' '+ampm+'<br /><a href="'+this.link+'">'+this.title+'</a></span><br />'+this.description.trunc(80,true)+'</p>'
                    var eventItem = '<div class="singleevent '+this.category+'" data-month="'+(startTime.getMonth()+1)+'"><strong>'+this.startdate+' - '+endHours+':'+endMinutes+' '+ampm+'</strong><p><a href="'+this.link+'">'+this.title+'</a><br />'+this.description.trunc(80,true)+'</p>';

                    if(limit==0) {
                        $('#eventlist').append(eventItem);
/*                        $('#eventlist').append('<p class="event" data-month="'+(startTime.getMonth()+1)+'"><span class="green">'+this.startdate+' - '+endHours+':'+endMinutes+' '+ampm+'<br /><a href="'+this.link+'">'+this.title+'</a></span><br />'+this.description.trunc(80,true)+'</p>');
*/
                    } else {
                        if(index < limit) {
                            $('#eventlist').append(eventItem);
                        } 
                    }
                    //var dates = this.date.split('|');
                    //var currentDate = new Date(dates[0]);
                    //this.date = currentDate;
                    // if(index <2 && $('#seconary #column2').length>0) {
                  });
//                  console.log('evtHandler.calData',evtHandler.calData);
        },
        onChangeDate: function(year,month,inst){
      //      console.log('onChangeDate',year,month,inst);
            $('#eventlist p.event').each(function(index) {
                if($(this).data('month') == month) {
                    $(this).css('display','block');
                }   else {
                    $(this).css('display','none');
                }
            })
        },
        onSelect: function(dateText) {
            var date,
                selectedDate = new Date(dateText),
                i = 0,
                event = null;

            /* Determine if the user clicked an event: */
            while (i < evtHandler.calData.length && !event) {
                date = new Date(evtHandler.calData[i].startdate);
                date.setHours(0,0,0,0);
               // console.log('onselect reached',selectedDate.valueOf(),date.valueOf());

                if (selectedDate.valueOf() === date.valueOf()) {
                    event = evtHandler.calData[i];
                }
                i++;
            }
            if (event) {
                /* If the event is defined, perform some action here; show a tooltip, navigate to a URL, etc. */
                window.location = event.link;

                //alert(event.title);
            }
        },
        beforeEventsDay: function(date) {
            var result = [true, '', null];
            var cat = ' ';
            var matching = $.grep(evtHandler.calData, function(event) {
                var evtDate = new Date(event.date);
                var calDate = new Date(date);
              if((evtDate.format('mm/dd/yy') === calDate.format('mm/dd/yy')) && (event.category != '')) {
                    cat += (event.category +' ');
                }
                return evtDate.format('mm/dd/yy') === calDate.format('mm/dd/yy');
//                return event.date.valueOf() === date.valueOf();
            });
        
            if (matching.length) {
                var style = "highlight"+cat;
                result = [true, style, null];
            }

            return result;
        }
    }
    var generations = {
        basePath: (window.location.hostname=='localhost') ? '/generations':'',
        init: function() {
            if($('#promoslide.flexslider').length > 0) {
                $('#promoslide.flexslider').flexslider({
                    slideshow:true,
                    slideshowSpeed: 5000,
                    pauseOnHover: true,
                    directionNav: false
                });
            }
            if($('#homeslide.flexslider').length > 0) {
                $('#homeslide.flexslider').flexslider({
                   slideshow:true,
                    useCSS : false,
                    slideshowSpeed: 7000,
                    pauseOnHover: true,
                    directionNav: false
                });
            }
            if($('#slideshow.flexslider').length > 0) {
                $('#slideshow.flexslider').flexslider({
                    slideshow: false,
                    directionNav: true
                });
            }
             // if($('.tabcontent').length>0) {
             //      $('.tabcontent').each(function() {
             //         $(this).jScrollPane();
             //      });
             // }

            if($('#tabs').length > 0) {
                $('#tabs').tabs({
                    activate: function( event, ui ) {
                       // console.log('HIT',ui);
                       // ui.newPanel.jScrollPane();
                    }

                });
            }
            if($('.show_hide').length > 0) {
                $('.show_hide').showHide();
            }
            if($('#calendar').length>0) {
                //console.log('CALENDAR ON');
                evtHandler.init();  
            }

            console.log('CHECKING MEMBER LOGIN');
            if($('form#member-login').length>0) {
                console.log('MEMBER LOGIN');
                $('#member-login input#UsernameField').inputToggle();
                $('form#member-login a.cta').click (function(e) {
                    var user = ($('#member-login input#UsernameField').val() != 'Username') ? $('#member-login input#UsernameField').val() : '';
                    if(user != '' && user != 'Username') {
                        document.forms['log'].submit();
                        return false;
                    } else {
                        e.preventDefault();
                    }
                    e.preventDefault();
                })
            }

            // if($('#member-login').length>0) {
            //     $('#member-login input#UsernameField').inputToggle();
            //     $('#member-login a.cta').click (function(e) {
            //         e.preventDefault();
            //         var user = ($('#member-login input#UsernameField').val() != 'Username') ? $('#member-login input#UsernameField').val() : '';
            //         if(user != '') document.forms['log'].submit(); return false;
            //     })
            // }




            // if($('.marketoform').length>0) {
            //     //console.log('FORM HERE');
            //     $('.marketoform input[type="text"]').inputToggle();
            //     $('.marketoform form').validate({
            //          debug: true
            //     });
            // }

            // if ($('a.cta-overlay, a.colorbox-link, a.overlay').length >0) {
            //     var frameWidth, frameHeight;
            //     $('a.cta-overlay, a.colorbox-link, a.overlay').each(function(index,val){
            //         var tarURL = $(this).attr('href');
            //         var urlCheck=tarURL.indexOf("pages.mygenfcu.org");
            //         if(urlCheck != -1) {
            //             var newURL = $(this).attr('href') + '?parenturl=' + window.location.href;
            //             $(this).attr('href',newURL);
            //             console.log('attr:',$(this).attr('href'));
            //         }

            //     });
            //     frameWidth = ($('body.page-template-page-templatesfinancial-education-php').length>0) ? '60%' : ($('#main.product-page').length>0) ? '45%' : '70%';
            //     frameHeight = ($('body.page-template-page-templatesfinancial-education-php').length>0) ? '80%' : ($('#main.product-page').length>0) ? '100%' : '150%';
            //     $('a.cta-overlay, a.colorbox-link, a.overlay').colorbox({
            //         iframe:true,
            //         innerWidth:920,
            //         innerHeight:600,
            //         onComplete: function() {
                        
            //             var targetURL = $('iframe.cboxIframe').attr('src');
            //             urlCheck=targetURL.indexOf('www.moneyedu.org');
            //             if(urlCheck != -1) {
            //                 console.log('opened');
            //                 $('iframe.cboxIframe').css('background-color','#fff');
            //             }
            //            // $.colorbox.resize();
            //         }

            //     });
            // }


            // if($('.alerts_widget').length>0) {
            //     $('.alerts_widget li.wp-cpl p').each(function(index,value){
            //         if($(this).find('a.read-more').length >0) {
            //             var rMore = $(this).find('a.read-more');
            //             var shrtText = $(this).text();
            //             $(this).text(shrtText.trunc(120, true));
            //             $(this).append(rMore);
            //         }
            //     })
            // }

            if($('#findLoc').length>0){
                mapHandler.initWidget();
            }

            // if($('#defaultmap_poi_list').length>0){
            //     mapHandler.initMapDD();
            // }
            
            if($('#googlemaps').length>0) {
                mapHandler.initEventMap();
            }
// LINK HANDLER FOR OVERLAYS AND EXTERNAL LINKS
            $('a').on('click', function(e){

               if($(this).hasClass('cta-overlay') || $(this).hasClass('colorbox-link') || $(this).hasClass('overlay')) {

                    var destURL = $(this).attr('href');
                    var formCheck = destURL.toLowerCase().indexOf('pages.mygenfcu.org') >= 0;
                    var slideCheck = destURL.toLowerCase().indexOf('www.slideshare.net') >= 0;

                    var moneyEduCheck = (destURL.toLowerCase().indexOf('moneyedu.org') >= 0);
                    var pdfCheck = destURL.toLowerCase().indexOf('.pdf') >= 0;
                    var frameWidth = (formCheck) ? 500 : (slideCheck) ? 800 : (moneyEduCheck) ? 760 : (pdfCheck) ? 920 : 760;
                    var frameHeight = (formCheck) ? 700 : (slideCheck) ? 700 : (moneyEduCheck) ? 600 : (pdfCheck) ? 600 : 900;

                    if (formCheck) {
                       destURL += ('?parenturl=' + window.location.href);
                    }

                    $.colorbox({
                        href:destURL, 
                        iframe: true,
                        scrolling: false,
                        innerWidth: frameWidth,
                        innerHeight: frameHeight,
                        onComplete: function() {
                            var targetURL = $('iframe.cboxIframe').attr('src');
                            urlCheck=(targetURL.indexOf('www.moneyedu.org')>=0)||(targetURL.indexOf('pages.mygenfcu.org')>=0);
                            if(urlCheck) {
                                $('iframe.cboxIframe').css('background-color','#fff');
                            }
                        }
                    });
                    e.preventDefault();

                } else {
                    //ADDS THE EXIT MESSAGE TO EXTERNAL LINKS
                    var allowedURLs = ['https://olb.mygenfcu.org/User/AccessSignup/Start', location.hostname];
                    var blankCheck = $(this).attr('target')=='_blank';

                    var curLocation = location.hostname;
                    var destURL = ($(this).attr('href').length > 0) ? $(this).attr('href') : '' ;
                    var URLcheck = destURL.toLowerCase().indexOf('mygenfcu.org') >= 0;
                    var httpCheck = destURL.toLowerCase().indexOf('http') >= 0;
                    //str.toLowerCase().indexOf("yes") >= 0
                    if(blankCheck && httpCheck && !URLcheck) {
                        e.preventDefault();
                        var r=confirm('You have requested an external link.\nIf you would like information regarding our Third-Party Link policy, please click Cancel, then click onto “Third-Party Links” at the bottom of this page.');
                        if (r==true) {
                            window.open($(this).attr('href'));
                        }
                        else {
                            // x="You pressed Cancel!";
                        }
                    } 
                }

            });

        //TOGGLE READ MORE/READ LESS BLOG FUNCTIONALITY
        $('.showhide').each(function() {
                var showHide = $(this);
            $(this).find('.excerpt a.read-more').click(function(e) {
                e.preventDefault();
                showHide.find('.fullcontent').removeClass('hide').addClass('show');
                showHide.find('.excerpt').removeClass('show').addClass('hide');
            })
            $(this).find('.fullcontent a.read-less').click(function(e) {
                e.preventDefault();
                $('html, body').animate({scrollTop: (showHide.offset().top -50)}, 0);
                showHide.find('.fullcontent').removeClass('show').addClass('hide');
                showHide.find('.excerpt').removeClass('hide').addClass('show');
            })

        });


////////////IE adjustments

            var mu = $.browser;
            if (mu.msie && mu.version < 9) {

              //  $('.menu-primary-navigation-container ul li:last-child, ul.menu li:last-child').css('border-right','none');
                // $('.menu-primary-navigation-container ul li:first-child, ul.menu li:first-child').css('border-left','none');
                // if($('.section-landing-page section:nth-of-type(even)').length >0) {
                //     $('.section-landing-page section:nth-of-type(even)').css('margin-right','0px');
                // }
                $('ul#menu-header-navigation li:last-child a').css('border-right','none');
                if($('ul.menu ul.sub-menu li:last-child a').length > 0) {
                    $('ul.menu ul.sub-menu li:last-child a').css('padding','0px 4px 4px 12px');
                }
               if($('#mainvis, #homeslide li.slide, .vidHolder .video').length > 0) {
                    $('#mainvis, #homeslide li.slide, .vidHolder .video').each(function(){
                        //$(this).css({backgroundSize: "cover"});
                        // var tDiv = $(this).css('background-image');
                        // tDiv = tDiv.replace('url(','').replace(')','');
                        // if(tDiv != '' && tDiv != undefined) {
                        //    // console.log('tDiv',tDiv);
                        //     $(this).css({
                        //          'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='+tDiv+', sizingMethod="scale")',
                        //          '-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='+tDiv+', sizingMethod="scale")'
                        //     });
                        // }
                    });
                }


            }
            $('.siteloader').css('display','none');

            //$('#main .post-box').css('visibility','visible');
        },
        mobileInit: function() {
    //  if( /Android|webOS|iPhone|iPod|BlackBerry|iemobile/i.test(navigator.userAgent) && !showMobile) {
            console.log('mobileInit: ',navigator.userAgent);
            console.log('site size: ',$(window).width() +'x' + $(window).height());
        if( /Android|webOS|iPhone|iPod|BlackBerry|iemobile/i.test(navigator.userAgent)) {
    //      use line below to include iPads
    //     if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|iemobile/i.test(navigator.userAgent) || urlParams.showMobile == 'true') {
            $(window).bind('orientationchange', function(event) {
                console.log('change: ',$(window).width() +'x' + $(window).height(),event);
            });
    
            $('body').addClass('mobile');
            generations.init();
        } else {
            $('body').addClass('desktop');
            generations.init();
        }
        //MOBILE TESTING ONLY
        //ADD PARAM: ?showMobile=true
//      if(urlParams.showMobile == 'true') {
//          $('body').addClass('mobile');
//          $('body').removeClass('desktop');
//      }
        
    }


    }

    $(document).ready(function() {
        generations.init();
            
    });

}(jQuery));