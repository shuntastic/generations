<div class='mapp-iw'>
	<?php echo $poi->get_thumbnail(array('class' => 'mapp-thumb')); ?>
	<div class='mapp-title'>
		<?php echo $poi->get_title_link(); ?>
	</div>
	<div class='mapp-body'>
		<?php echo $poi->get_body(); ?>
	<span class="directionlink" style='float:left;clear:both;'>
		<?php echo $poi->get_links(); ?>
	</span>
	</div>
</div>