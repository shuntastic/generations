<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package required+ Starter
 * @since required+ Starter 0.1.0
 */
?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=1120px, minimum-scale=.4, maximum-scale=device-width">
    <!--[if IE 9]> <meta http-equiv="X-UA-Compatible" content="IE=9" /> <![endif]-->
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico?v=2" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/images/devices/apple-touch-icon-iphone.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/images/devices/apple-touch-icon-iphone.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/images/devices/apple-touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/images/devices/apple-touch-icon-ipad.png" />
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
	<?php 
			if (!is_home()) {
				$tab_review = simple_fields_value('reviewID'); 
				if ( $post->ID == 382) {
				echo '<script type="text/javascript" src="http://generationsfcu.ugc.bazaarvoice.com/static/4523-en_us/bvapi.js"></script><script type="text/javascript"> $BV.ui("submission_container", {userToken: ""});</script>';
				} else if ($tab_review){
					echo '<script type="text/javascript" src="//generationsfcu.ugc.bazaarvoice.com/static/4523-en_us/bvapi.js"></script>
						<script type="text/javascript"> $BV.configure("global", {submissionContainerUrl: "http://www.mygenfcu.org/submissionpage/"}); $BV.ui("rr", "show_reviews", { productId:'.$tab_review.'});</script>';	
				}
			}
	?>
	<!--[if lt IE 9]>
	   	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" />
 		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/selectivizr.js"></script>
	<![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/javascripts/plugins.js"></script>
</head>
<body <?php body_class(); ?>>
	<?php
		// global $post;
		 $pageID = $post->ID;
		 $currentDepth = count(get_post_ancestors($pageID));
		 $parent = $post->post_parent;

		 if($currentDepth == 1) {
			$nextPage = next_page_not_post('Previous Page', 'true', 'sort_column=menu_order&child='.$pageID.'&sort_order=desc');
			$prevPage = previous_page_not_post('Next Page', 'true', 'sort_column=menu_order&child='.$pageID.'&sort_order=desc');
		 } else if ($currentDepth ==2) {
			$nextPage = next_page_not_post('Previous Page', 'true', 'sort_column=menu_order&child='.$pageID.'&sort_order=desc');
			$prevPage = previous_page_not_post('Next Page', 'true', 'sort_column=menu_order&child='.$pageID.'&sort_order=desc');

		 } else {
			$nextPage = next_page_not_post('Previous Page', 'true', 'sort_column=menu_order&parent='.$parent.'&sort_order=desc');
			$prevPage = previous_page_not_post('Next Page', 'true', 'sort_column=menu_order&parent='.$parent.'&sort_order=desc');
		 }
		 if (!is_home()) {
		 	if ($pageID == 9) {
		 		$nextPage = '<a href="/" title="Generations Home">Next Page</a>';
		 	}
			 if (!empty($nextPage) || !empty($prevPage)) {
				echo '<div class="page-arrows">';
				if (!empty($nextPage)) echo '<div class="prev">'.$nextPage.'</div>';
				if (!empty($prevPage)) echo '<div class="next">'.$prevPage.'</div>';
				echo '</div>';
			 }
		}
	?>
    <!-- <div class="page-arrows"><div class="prev"><a href="javascript:void(0);">Previous Page</a></div><div class="next"><a>Next Page</a></div></div> -->
	<?php 
		/*if (!is_home()) {
			echo '<div class="page-arrows"><a class="prev">Previous Page</a><a class="next">Next Page</a></div>';
		}*/
	?>
	<!-- Start the main container -->
	<div id="container" class="container" role="document">
		<?php
			/**
			 * Include the Foundation Top Bar
			 *
			 * It uses the same navigation as nav.php
			 * so you might want to use a different navigation
			 * here.
			 */
			if ( is_page_template( 'page-templates/off-canvas-page.php' ) ) {
				get_template_part('nav', 'top-bar');
			}
		?>
		<!-- Row for blog navigation -->
		<div class="row">
			<header id="nav-header" class="twelve columns required-header" role="banner">
					<div class="columns">
						<?php wp_nav_menu( array(
							'theme_location' => 'meta',
							'container' => false,
							'menu_class' => 'inline-list right',
							'fallback_cb' => false
						) ); ?>
						<?php
							get_search_form();
						?>
					</div>
				<?php
					/**
					 * Include our custom-header.php
					 *
					 * Used with the header image stuff.
					 */
					get_template_part( 'custom-header' );
				?>
				<?php
					/**
					 * Include the default navigation
					 *
					 * You could easily do something like:
					 * if ( is_front_page() ) {
					 * 	get_template_part( 'nav', 'front-page' ); // nav-front-page.php
					 * } else {
					 * 	get_template_part( 'nav' );	// nav.php
					 * }
					 */
					if ( ! is_page_template( 'page-templates/off-canvas-page.php' ) ) {
						get_template_part( 'nav' );
					}
				?>
			</header>
		</div><!-- // header.php -->
		<div class="siteloader"><span><img src="<?php echo get_template_directory_uri(); ?>/images/mainloader.gif" /></span></div>