CREATE TABLE IF NOT EXISTS `wp_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_description` varchar(255) NOT NULL,
  `device_regex` varchar(32) NOT NULL,
  `redirect_url` varchar(1024) NOT NULL,
  `redirect_homepage` int(11) NOT NULL DEFAULT '0',
  `redirect_force` int(11) NOT NULL DEFAULT '0',
  `redirect_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;