<?php

/**
 * @author Webarto 
 * @name View PHP Class
 * @package Wordpress
*/

class View
{
    public static function factory($view, $variables = array())
    {
        $html = file_get_contents(__ROOT__.'/views/'.$view.'.php');
        foreach($variables as $key => $value)
            $html = str_replace("{\$$key}", $value, $html);
        return $html;
    }
}

?>