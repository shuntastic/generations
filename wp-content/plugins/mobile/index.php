<?php

/**
 * @package WordPress Mobile Redirect
 * @version 1.9
 */

/**
Plugin Name: Mobile Redirect
Plugin URI: http://codecanyon.net/item/wordpress-mobile-redirect-plugin/1476160?ref=wp-admin
Description: Wordpress Mobile Redirect is a lightweight and customizable plugin that can detect most of popular internet enabled mobile devices and redirect them.
Author: Webarto
Version: 1.9
Author URI: http://webarto.com
*/

include_once 'init.php';

register_activation_hook(__FILE__, 'mr_install');
register_deactivation_hook(__FILE__, 'mr_uninstall');

function mr_install()
{
    DB::query("DROP TABLE IF EXISTS wp_mobile");
    $query = View::factory('../admin/create_table');
    DB::query($query);

    $query = View::factory('../admin/fill_table');
    DB::query($query);
}

function mr_uninstall()
{
    DB::query("DROP TABLE wp_mobile");
}

function mr_redirect()
{

        # Nothing to see here, move on.
        if($_SESSION['m2w'] == true)
            return;
        
        # Prevent infinite redirect
		$href = "http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}";
        $href = trim($href, '/');
        $results = DB::get_results("SELECT id FROM wp_mobile WHERE redirect_url LIKE '$href'");
        if( ! empty($results))
			return;
   
        # Reset
        $x = array();

        # Read user agent from browser request
        $agent = $_SERVER['HTTP_USER_AGENT'];

        # U-A string empty, no point continuing
        if (empty($agent))
            return;
        # Check for Facebook scrapper
        if (strpos($agent, 'facebook') !== false)
            return;
        
        # Check
        $results = DB::get_results('SELECT * FROM wp_mobile WHERE redirect_url != "" AND device_regex != "Mobile"');
        
        # Check Mobile2Web
        foreach($results as $row)
        {
            if(stripos($_SERVER['HTTP_REFERER'], $row['redirect_url']) !== false AND $row['redirect_force'] == 0)
            {
                $_SESSION['m2w'] = true;
                return;
            }
        }
        
        # Check for device
        foreach($results as $row)
        {
            if(stripos($agent, $row['device_regex']) !== false)
            {
                $x = $row;
                break 1;
            }
        }
                
        # Redirect if device is mobile
        if ( ! empty($x))
        {
            # Check for homepage only redirect
            if ($x['redirect_homepage'] == 1)
                if ($_SERVER['REQUEST_URI'] != '/') 
                    return;
            
            DB::query("UPDATE wp_mobile SET redirect_count = redirect_count + 1 WHERE id = '{$x[id]}' LIMIT 1");
            wp_redirect($x['redirect_url']);
            exit;
        }
        
        return false;

}