<?php

function mr_option_menu()
{
    add_menu_page( PLUGIN_NAME, '<span>'.PLUGIN_NAME.'</span>', 8, 'mr-settings',  'mr_settings', plugin_dir_url(__FILE__).'icon.png');    
}

function mr_settings()
{

    if (isset($_POST['submit']))
    {
        $count = count($_POST['id']);
        for ($i = 1; $i <= $count; $i++)
        {
            if ( ! empty($_POST['redirect_url'][$i]))
                $_POST['redirect_url'][$i] = mysql_real_escape_string($_POST['redirect_url'][$i]);
                
            $_POST['redirect_force'][$i] =  (int) $_POST['redirect_force'][$i];
            $_POST['redirect_homepage'][$i] =  (int) $_POST['redirect_homepage'][$i];
            
            DB::query("UPDATE wp_mobile SET 
            redirect_url = '{$_POST[redirect_url][$i]}',
            redirect_force = {$_POST[redirect_force][$i]},
            redirect_homepage = {$_POST[redirect_homepage][$i]}
            WHERE id = '{$_POST[id][$i]}'");
        }
    }
    
    $result = DB::get_results('SELECT * FROM wp_mobile');
    foreach ($result as $row)
    {
        $table .= View::factory('tr', $row);
    }
    echo View::factory('index', array('table' => $table, 'uri' => __URI__));

}

function mr_analytics()
{
    $i = 0;
    $result = DB::get_results('SELECT * FROM wp_mobile WHERE redirect_count > 0');
    foreach ($result as $row)
    {
        $row['id'] = $i;
        $script .= "data.setValue($i, 0, '$row[device_regex] ($row[redirect_count])');data.setValue($i, 1, $row[redirect_count]);";
        $i++;
    }
    return View::factory('analytics', array('script' => $script, 'rows' => $i));
}