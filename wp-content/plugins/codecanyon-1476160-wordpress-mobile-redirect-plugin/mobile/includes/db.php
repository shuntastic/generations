<?php

/**
 * @author Webarto 
 * @name Wordpress DB Wrapper PHP Class
 * @package Wordpress
*/

class DB
{

    public static function get_results($query)
    {
        global $wpdb;
        $query = self::set_prefix($query);
        $result = $wpdb->get_results($query, ARRAY_A);
        return $result;      
    }

    public static function query($query)
    {
        global $wpdb;
        $query = self::set_prefix($query);
        $result = $wpdb->query($query);    
        return $result;
    }
    
    public static function set_prefix($query)
    {
        global $wpdb;
        return str_replace('wp_', $wpdb->prefix, $query);        
    }
    
}

?>