<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type='text/javascript'>
    google.load('visualization', '1', {packages:['imagepiechart']});
</script>
<style type="text/css">
<!--
img{vertical-align:middle !important;}
#wa-wrapper{border-radius:8px;background:#ECECEC url("{$uri}/static/gfx/wrapper.png") repeat-y left top;overflow:hidden;margin-right:15px;border:1px solid #CCCCCC;}
#wa-menu{float:left;position:relative;width:200px;}
#wa-content{margin:20px 20px 20px 220px;background:#fff;padding:20px;border:1px solid #F9F9F9;}

#wa-menu li{margin:0 !important;}
#wa-menu li a img{margin-right:10px;}
#wa-menu li a{color:#fff;border-bottom: 1px solid #333;box-shadow: 0 1px 0 rgba(255, 255, 255, 0.08);padding:10px;font-size:16px;display:block;color:#ccc !important;text-shadow: 0 1px 0 rgba(0, 0, 0, 0.6) !important;font-weight:bold;text-decoration:none;}
#wa-menu li a:hover{color:#fff !important;background:#464646 url({$uri}/static/gfx/oblique.png) repeat;}

.wa-clear{visibility:hidden;clear:both;}

.wa-center{text-align:center;}
.wa-submit{width:400px;margin-top:20px;}

.wa-table{width:100%;border-color:#DFDFDF;border-collapse:collapse;}
.wa-table thead th{background-color: #F1F1F1;background-image:-moz-linear-gradient(center top , #F9F9F9, #ECECEC);}
.wa-table td,th{border-color:#DFDFDF;border-style:solid;border-width:1px !important;border-bottom-color:#DFDFDF;border-top-color:#FFFFFF;padding:7px;}
.wa-table th{border-top-color: #DFDFDF;}

.wa-table tr:nth-child(even) td {background: #F9F9F9}
.wa-table tr:nth-child(odd) td{background: #FFF}
.wa-table td{text-align:center;}

.wa-intro{font-size: 10px; color: #fff; line-height:normal; text-align: justify;padding: 10px;}
.wa-copy{font-size:12px;color:#fff;text-align:center;padding:10px;}
-->
</style>

    <h1>Mobile Redirect</h1>
       
    <div id="wa-wrapper">
    
    <div id="wa-menu">
    
    <div>
    <img alt="" src="{$uri}/static/gfx/wa.png" />
    </div>
    
    <div>
        <ul>
            <li><a href="?page=mr-settings"><img alt="" src="{$uri}/static/gfx/redirect.png" /> Redirects</a></li>
            <li><a href="javascript:load('{$uri}/ajax.php?__a=1&__page=analytics', '#wa-content');"><img alt="" src="{$uri}/static/gfx/analytics.png" /> Analytics</a></li>
        </ul>
    </div>
    
    <div class="wa-intro">
    Wordpress Mobile Redirect/Detect is a small plugin that can detect most of popular internet enabled mobile devices and redirect according to URL you provide. It is also easy to customize redirect URL's and devices.
    </div>
    
    <div class="wa-copy">Powered by <a href="http://webarto.com">&#9733;Webarto</a></div>
    
    </div>
    
    <div id="wa-content">
    
    <form method="post" action="">
    
    <table class="wa-table">
      <thead>
        <tr>
          <th width="30px">
            <span>ID <a href="javascript:;" title="Database ID, not important for You.">[?]</a></span>
          </th>
          <th>
            <span>Device <a href="javascript:;" title="Name of the device or device group.">[?]</a></span>
          </th>
          <th>
            <span>URL <a href="javascript:;" title="URL to redirect to. Please type in EXACT URL. To disable redirect just delete URL from input field.">[?]</a></span>
          </th>
          <th>
            <span>Homepage <a href="javascript:;" title="Redirect users ONLY when they visit homepage first.">[?]</a></span>
          </th>
          <th>
            <span>Force <a href="javascript:;" title="Do not allow user to go back after being redirected to mobile site.">[?]</a></span>
          </th>
          <th>
            <span>Redirects <a href="javascript:;" title="Number of redirects. Double click to reset!">[?]</a></span>
          </th>
        </tr>
      </thead>
      <tbody>
        {$table}
      </tbody>
    </table>
    
    <center><input class="wa-submit button-primary" type="submit" tabindex="5" value="Save" class="button-primary" name="submit" /></center>
    
    </form>
    
    <hr class="wa-clear"/>
        
    </div>
    <hr class="wa-clear"/>
    </div>

<script type="text/javascript">
function load(url, selector)
{
    jQuery(selector).load(url, function() {

    });
}

function get(url)
{
    return jQuery.ajax({url: url, async: false}).responseText;
}

jQuery(document).ready(function() {
    
    jQuery('.wa-table input').attr('autocomplete', 'off');
    jQuery('.wa-table input[data-checked=1]').attr('checked', true);

    jQuery('.post-com-count').dblclick(function() {
        jQuery(this).closest('div').load('{$uri}/ajax.php?__a=1&__page=reset&id=' + jQuery(this).data('id'));
    });

});
</script>