<div id="chart_div" align="center"></div>
<script type="text/javascript">
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Device');
        data.addColumn('number', 'Redirect Count');
        data.addRows({$rows});

        {$script}

        var chart = new google.visualization.ImagePieChart(document.getElementById('chart_div'));
        chart.draw(data, {width: 760, height: 390, title: 'Mobile Redirect Analytics'});
</script>
