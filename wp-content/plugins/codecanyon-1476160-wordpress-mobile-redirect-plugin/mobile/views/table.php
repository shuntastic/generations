    <h1>Mobile Redirect</h1>
    
    <div style="margin-bottom: 10px;">
    Wordpress Mobile Redirect/Detect is a small plugin that can detect most of popular internet enabled mobile devices and redirect according to URL you provide. It is also easy to customize redirect URL's and devices. There is a counter on the right of every device name, so you know how many users are redirected. Please type in full URL. To disable redirect, just delete URL.
    </div>
        
    <form method="post" action="">
    
    <input style="margin-bottom: 10px;" type="submit" tabindex="5" value="Save" class="button-primary" name="submit" /> 
    
    <table cellspacing="0" class="wp-list-table widefat fixed posts">
      <thead>
        <tr>
          <th style="" width="50px" class="manage-column" id="cb" scope="col">
            <span>ID</span>
          </th>
          <th style="" width="200px" class="manage-column" id="device" scope="col">
            <span>Device</span>
          </th>
          <th style="" width="500px" class="manage-column" id="url" scope="col">
            URL
          </th>
          <th style="" class="manage-column" id="comments" scope="col">
            <span>Redirects</span>
          </th>
        </tr>
      </thead>
      <tbody id="the-list">
        {$table}
      </tbody>
    </table>
    </form>