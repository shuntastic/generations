<?php

session_start();

define('__URI__', plugin_dir_url(__FILE__));
define('__ROOT__', plugin_dir_path(__FILE__));
define('PLUGIN_NAME', 'Mobile Redirect');

if ( ! class_exists('View'))
    include_once 'includes/view.php';
if ( ! class_exists('DB'))
    include_once 'includes/db.php';

if (is_admin())
{
	require_once __ROOT__.'/admin/index.php';
	add_action('admin_menu', 'mr_option_menu');
}
else
{
    add_action('init', 'mr_redirect');
}