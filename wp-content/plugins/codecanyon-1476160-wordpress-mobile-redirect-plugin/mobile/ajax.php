<?php

define('DOING_AJAX', true);
define('WP_ADMIN', true);

require_once('../../../wp-load.php');
include_once 'init.php';

$__a = (int) $_GET['__a'];
$__page = $_GET['__page'];

if($__a == 1)
{
    
    if($__page == 'analytics')
    {
        echo mr_analytics();
        die;
    }
    
    if($__page == 'reset')
    {
        $id = (int) $_GET['id'];
        DB::query("UPDATE wp_mobile SET redirect_count = 0 WHERE id = $id");
        echo View::factory('reset_count', array('id' => $id));
        die;
    }

}