=== WP Social SEO Booster - Knowledge Graph Social Signals SEO ===
Author URI: http://www.danielwaser.com
Plugin URI: http://wpsocial.com
Donate link: http://wpsocial.com/product/wp-social-seo-booster-pro/
Tags: seo, social seo, social, wp social, knowledge graph, social media, google rich snippets, open graph, microdata, reviews, author, twitter card, itemprop, software reviews, recipes, google, facebook, twitter, plugin, rating, star rating, image optimizer, kraken image optimizer, google plus
Requires at least: 3.3
Tested up to: 3.5.1
Stable tag: 1.1.0

Turn on Your Blog's Social SEO with WP Social SEO Booster and get a Huge Boost in Google Rankings!

== Description ==

WP Social SEO Booster adds Facebook Open Graph, Twitter Card and Google Rich Snippets (Microdata / schema.org) to your site to boost your sites search engine visibility. It is the only plugin which does include ALL the different types of microdata which are supported by Google.

**Features of this plugin include:**

* Works with any SEO Plugin, such as WordPress SEO by Yoast, All in One SEO and any other SEO plugin.
* Google Rich Snippets (Microdata) Integration - Automatically implements the Rich Snippets Tagging for all of your Content.
* Google Authorship - Seamlessly integrates Google Authorship for Single and Multi-Author blogs.
* Facebook Open Graph - Automatically optimizes your blog for Facebook Open Graph SEO.
* Twitter Meta Tags - Automatically adds the Twitter Meta Tags for increased connections between Google, Twitter, and your content.
* Increase SEO Rank - The Plugin does this all seamlessly and can take a poor performing blog to a Highly Ranked Blog FAST!
* Supports Rich Snippets (Microdata) for Reviews, Products, Business, People, Recipes, Software, Videos, Events.
* Supports Google Plus Publisher Pages.
* Supports All Open Graph Content Types such as Article, Book, Profile, Website, Music Album, Song, Movie, TV Show, Video.
* Adds the HTML5 Boilerplate optimized .htaccess entries to your .htaccess file.
* Does include a star rating system which works with the author rating together.
* Every option can be set for general use as well as customized for each post you create.

We also have a pro version for that plugin with even more features at [WP Social Seo Booster Pro](http://wpsocial.com/product/wp-social-seo-booster-pro/) and check out, what our customers say about this plugin.

[youtube http://www.youtube.com/watch?v=k7n2qS1T16o]


**Languages**

WP Social SEO Booster as been translated into the following languages:

1. English
2. German

Would you like to help translate the plugin into more languages? [Contact Daniel](http://wpsocial.com/contact-developer/).


== Installation ==

1. Upload the wpsocial-seo-booster folder to the /wp-content/plugins/ directory
2. Activate the WP Social SEO Booster plugin through the 'Plugins' menu in WordPress
3. Configure the plugin by going to the WP Social SEO Booster menu which can be found under Settings


== Frequently Asked Questions ==

= Can this plugin be used with any theme? =

Yes, you can use the plugin with any theme.

= What SEO Plugins will WP Social SEO Booster work with? Are there Compatibility issues? =

SEO Booster will work with ANY SEO plugin because it does scan the code of your site and if it finds a title and/or a meta description tag within the header it will take these entries automatically and use it for all the different Google, Open Graph & Twitter meta tags. And if you want to use other entries for the social part then you use for SEO you can do that by filling out the fields within the WP Social SEO Booster meta box.

= Are there training videos or manuals for this plugin? =

Yes we have a manual and videos included within the plugin settings.


== Screenshots ==

1. SEO Booster Performance Settings
11. SEO Booster Open Graph Settings
12. SEO Booster Google Authorship Settings
13. SEO Booster Kraken Image Optimizer Settings
14. SEO Booster Star Rating Settings
15. SEO Booster Flush Rating Settings
16. SEO Booster Meta Box (Post) Business
17. SEO Booster Meta Box (Post) Event
18. SEO Booster Meta Box (Post) Open Graph
2. SEO Booster Meta Box (Post) People
3. SEO Booster Meta Box (Post) Product
4. SEO Booster Meta Box (Post) Recipe
5. SEO Booster Meta Box (Post) Review
6. SEO Booster Meta Box (Post) Software
7. SEO Booster Meta Box (Post) Video
8. Rich Snippets Author
9. Rich Snippets Reviews


== Changelog ==
= Version 1.1.0 (2013-05-03) =
* [*] Removed Kraken Image Optimizer
= Version 1.0.9 (2013-05-03) =
* [*] Fixed the 1 star and 2 stars rating from not showing up within the Reviews
* [*] Updated the language .po file
* [*] Updated German translation
= Version 1.0.8 (2013-05-01) =
* [*] Hiding the price on reviews if empty
= Version 1.0.7 (2013-04-24) =
* [*] Added better support for All in One SEO plugin
* [*] Disabled content scanning option, if WordPress SEO or All in One SEO are installed to prevent conflicts with other plugins which do also scan the content
= Version 1.0.6 (2013-04-12) =
* [*] Added option to link email address within Business vCard
= Version 1.0.5 (2013-04-02) =
* [*] Updated software Rich Snippets
= Version 1.0.4 (2013-03-30) =
* [*] Updated Rich Snippets code for ratings
* [*] Fixed CSS issue
= Version 1.0.3 (2013-03-23) =
* [*] Updated code for better integration with the WordPress SEO plugin by Yoast
= Version 1.0.2 (2013-03-15) =
* [*] Added shortcode for rating stars
= Version 1.0.1 (2013-01-19) =
* [*] Fixed Image Uploader issue
= Version 1.0.0 (2013-01-06) =
* [*] Initial release