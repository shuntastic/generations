<?php
/*
Plugin Name: (JumpMobi) Mobile Redirect
Plugin URI: http://www.jumpmobi.com
Description: This feature rich WordPress Mobile Redirect Plugin detects mobile devices and redirects visitors to a specific URL, with multiple configuration options - Visit <a href="http://www.jumpmobi.com" target="_blank">JumpMobi.com</a> for other mobile related tools and themes.
Version: 2.2.1
Author: Jay Moreno (JumpMobi, LLC)
Author URI: http://www.jumpmobi.com
Copyright: (C) 2011 JumpMobi, LLC
This program is not free software: You cannot redistribute it, or resell it without the express permission of JumpMobi, LLC.
*/
// Check for plugin updates
set_site_transient( 'update_plugins', null );
include_once( 'class-custom-plugin-updater.php' );
$jumpmobi_redirect_plugin = new Custom_Plugin_Updater( 'http://www.jumpmobi.com/wp-updater/api/', __FILE__, array( 'licence_key' => get_option( 'mdp_update_access_key' ) ) );
?>
<?php
function jumpmobi_init(){
// Don't redirect if admin or logining in!
if( ! is_admin() AND ! strpos($GLOBALS['pagenow'], 'login.php') ) {
// Redirect if mobile
$detectie = strtolower($_SERVER['HTTP_USER_AGENT']);
if(strpos($detectie,'msie')) {$msie="yes";}
if(strpos($detectie,'iemobile')) {$iemobile="yes";} else {$iemobile="no";} 
if ($msie == "yes" && $iemobile == "no") {} else {
if (request_is_mobile()) {
$detection_type = get_option('jumpmobi_detection_type');
if ($detection_type == "php" ){ 
		$redirect = get_option('jumpmobi_redirect_url');
		$header_redirect ="Location: $redirect";
		$returning_visitors = get_option('jumpmobi_returning_visitors');
				if ($returning_visitors == "yes" )
						{
							if (!isset($_COOKIE["mobivisitor"]) || ($_COOKIE['mobivisitor'] == "yes")){
							$header_cookie = setcookie("mobivisitor", "yes", time()+900);
							$header ="Location: $redirect";
							header($header);
							exit();
						}}
				if ($returning_visitors == "no" )
						{
							if (!isset($_COOKIE["mobivisitor"])){
							$header_cookie = setcookie("mobivisitor", "no", time()+900);
							$header ="Location: $redirect";
							header($header);
							exit();
						} else if ($_COOKIE['mobivisitor'] == "no"){}} 
			}}
	}
}
$jumpmobi_detection_type = get_option('jumpmobi_detection_type');		
$returning_visitors = get_option('jumpmobi_returning_visitors');
if (request_is_mobile() && ($returning_visitors == "yes") && ($detection_type == "java")){setcookie("mobivisitor", "yes", time()+900);}
if (request_is_mobile() && ($returning_visitors == "no") && ($detection_type == "java")){setcookie("mobivisitor", "no", time()+900);}
} //end function

if( function_exists('jumpmobi_init') )
	add_action('init', 'jumpmobi_init');


function jumpmobi_iframedetect() {

// Don't redirect if admin or logining in!

if( ! is_admin() AND ! strpos($GLOBALS['pagenow'], 'login.php') ) {

// Redirect if iframe 

$iframetoggle = get_option('jumpmobi_iframe_toggle');

if ($iframetoggle == "true" ){ 

?>

<script type="text/javascript">

if (window.self === window.top) {

} else {

<?php

$redirect = get_option('jumpmobi_redirect_url');

?>

	window.location.href = '<?php echo "$redirect"; ?>';

}

</script>

<?php }

// Redirect using javascript

$jumpmobi_detection_type = get_option('jumpmobi_detection_type');	
				
if ($jumpmobi_detection_type == "java" ){ 
	
if (!isset($_COOKIE["mobivisitor"]) || ($_COOKIE['mobivisitor'] == "yes")){

		$redirect = get_option('jumpmobi_redirect_url');

		$screensize = get_option('jumpmobi_screensize');

		$popupalert = get_option('jumpmobi_alert');

		$popuptxt = get_option('jumpmobi_alert_text');

		$ipad_detect = get_option('jumpmobi_ipad_toggle');



		if ($ipad_detect != "false" ){

			if ($popupalert != "false" ){ ?>

				<script language="javascript">

					if(navigator.userAgent.match(/iPad/i))

						{

							if(confirm('<?php echo "$popuptxt"; ?>')) 

						{

							window.location.replace('<?php echo "$redirect"; ?>');

						}}

				</script>

		<?php } else { ?>

				<script type="text/javascript">

					if(navigator.userAgent.match(/iPad/i))

						window.location.replace('<?php echo "$redirect"; ?>');

				</script>

		<?php }} ?>

		<?php

			if ($popupalert != "false" ){ ?>

				<script language="javascript">

					if (screen.width <  <?php echo "$screensize"; ?>)

					{

					if(confirm('<?php echo "$popuptxt"; ?>')) 

					{

						window.location.replace('<?php echo "$redirect"; ?>');

					}}

				</script>

		<?php } else { ?>

			<script type="text/javascript">

				if (screen.width <  <?php echo "$screensize"; ?>)

					window.location.replace('<?php echo "$redirect"; ?>');

			</script>

		<?php }

		}

		}

		} // end javascript redirect

		} // end if cookieset



add_action('wp_head', 'jumpmobi_iframedetect' );

function request_is_mobile() {
// Don't check if admin or logining in!
if( ! is_admin() AND ! strpos($GLOBALS['pagenow'], 'login.php') ) {
  global $_request_is_mobile;
  if (!isset($_request_is_mobile)) {
    $_request_is_mobile = _request_is_mobile();
  }
  return $_request_is_mobile;
}}
function _request_is_mobile() {
// Don't check if admin or logining in!
	if( is_admin() AND strpos($GLOBALS['pagenow'], 'login.php') ) {} else {
  if (get_http_header('X-Wap-Profile')!='' || get_http_header('Profile')!='') {
    return true;
  }
  if (stripos(get_http_header('Accept'), 'wap') !== false) {
    return true;
  }
  $user_agent = strtolower(get_http_header('User-Agent'));
  $ua_prefixes = array(
    'w3c ', 'w3c-', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq',
    'bird', 'blac', 'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco',
    'eric', 'hipt', 'htc_', 'inno', 'ipaq', 'ipod', 'jigs', 'kddi', 'keji',
    'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-', 'lg/u', 'maui', 'maxo', 'midp',
    'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-', 'newt', 'noki',
    'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox', 'qwap', 'sage',
    'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar', 'sie-',
    'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
    'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi',
    'wapp', 'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-'
  );
  if (in_array(substr($user_agent, 0, 4), $ua_prefixes)) {
    return true;
  }
$ipad_detect = get_option('jumpmobi_ipad_toggle');
if ($ipad_detect != "false" ){ 
$ua_keywords = array(
'android', 'blackberry', 'hiptop', 'ipod', 'lge vx', 'midp', 
    'maemo', 'mmp', 'netfront', 'nintendo DS', 'novarra', 'openweb',
    'opera mobi', 'opera mini', 'palm', 'psp', 'phone', 'smartphone',
    'symbian', 'up.browser', 'up.link', 'wap', 'windows ce', 'nokia',
	'samsung', 'motorola', 'lg', 'sie', 'ipad' 
  );
} else {
$ua_keywords = array(
'android', 'blackberry', 'hiptop', 'ipod', 'lge vx', 'midp', 
    'maemo', 'mmp', 'netfront', 'nintendo DS', 'novarra', 'openweb',
    'opera mobi', 'opera mini', 'palm', 'psp', 'phone', 'smartphone',
    'symbian', 'up.browser', 'up.link', 'wap', 'windows ce', 'nokia',
	'samsung', 'motorola', 'lg', 'sie' 
  );
}
  if (preg_match("/(" . implode("|", $ua_keywords) . ")/i", $user_agent)) {
    return true;
  }
  return false;
}}

function get_http_header($name, $original_device=true, $default='') {
// Don't check if admin or logining in!
if( ! is_admin() AND ! strpos($GLOBALS['pagenow'], 'login.php') ) {
  if ($original_device) {
    $original = get_http_header("X-Device-$name", false);
    if ($original!=='') {
      return $original;
    }
  }
  $key = 'HTTP_' . strtoupper(str_replace('-', '_', $name));
  if (isset($_SERVER[$key])) {
    return $_SERVER[$key];
  }
  return $default;
}}
function getDevice()
{
$u_agent = $_SERVER['HTTP_USER_AGENT'];
// Check To See If User Agent is iPad
if(preg_match('/iPad/i',$u_agent))
    {
    return $device = "iPad";
    }
} 
// Setup Backend Options
include 'jumpmobi-redirect-adminpage.php';
$jmredirect = new JMSubPage('plugins', 'JumpMobi Redirect');
$jmredirect->addParagraph('<b>Version: 2.2</b><br />
<b>Last Updated: December 2011</b><br /><br />
This feature rich plugin is used to redirect mobile visitors to a specific URL of your choosing. Click the "Help" tab located in the top right screen for more detailed information on each feature.<br /><br />
Visit <a href="http://www.jumpmobi.com" target="_blank">www.JumpMobi.com</a> for other mobile related tools and themes - be sure to signup to our newsletter for Beta Testing invites!<br /><br /><b>PLEASE NOTE:</b> Some of the Javascript features will not work if you are still logged in as admin - this is to ensure you can still log in to your WordPress control panel and not be redirected to your mobile site!');
// Redirect URL
$jmredirect->addInput(array(
		'id' => 'jumpmobi_redirect_url',
		'label' => 'Redirect URL',
		'desc' => 'Enter the URL you want to redirect mobile visitors too. Example: http://m.jumpmobi.com',
			'standard' => 'http://m.jumpmobi.com',	
	));
// Detection Type
$jmredirect->addDropdown(array(
		'id' => 'jumpmobi_detection_type',
		'label' => 'Detection Type',
		'desc' => 'Choose between PHP User Agent detection or Javascript Screen Size Detection',
		'standard' => 'php',
		'options' => array(
			'PHP User Agent Detection' => 'php',
			'Javascript Screen Size Detection' => 'java',
		),
	));
$jmredirect->addInput(array(
		'id' => 'jumpmobi_screensize',
		'label' => 'Set Screen Size Detection',
		'desc' => 'Screen sizes less than this size will be redirected using Javascript Detection Only - 1024 Recommended. <b>Do Not Include "px".</b><br />',
		'standard' => '1024',	
	));
// IPad Detection
$jmredirect->addCheckbox(array(
		'id' => 'jumpmobi_ipad_toggle',
		'label' => 'iPad Detection',
		'desc' => 'Toggle iPad detection and redirection.',
		'standard' => false,
	));
// Iframe Detection
$jmredirect->addCheckbox(array(
		'id' => 'jumpmobi_iframe_toggle',
		'label' => 'IFrame Detection',
		'desc' => 'Toggle IFrame detection and redirection.',
		'standard' => false,
	));
// Popup Alert
$jmredirect->addCheckbox(array(
		'id' => 'jumpmobi_alert',
		'label' => 'Popup Alert',
		'desc' => 'Toggle Popup Alert - <b>Note: Only works with Javascript Option Enabled.</b>',
		'standard' => false,
	));
// Redirect URL
$jmredirect->addInput(array(
		'id' => 'jumpmobi_alert_text',
		'label' => 'Popup Alert Text ',
		'desc' => 'Enter the text you want to display in the popup alert - <b>Note: Only works with Javascript Option Enabled.</b>',
			'standard' => 'Mobile Device Detected - Visit Mobile Site?',	
	));
// Redirect Returning Visitors
$jmredirect->addDropdown(array(
		'id' => 'jumpmobi_returning_visitors',
		'label' => 'Redirect Returning Visitors',
		'desc' => 'Redirect returning mobile visitors back to the mobile site.',
		'standard' => 'no',
		'options' => array(
			'Yes' => 'yes',
			'No' => 'no',
		),
	));
// Help Information	
$jmredirect->addHelp('<h2>JumpMobi Mobile Redirect Plugin For WordPress</h2>
<b>Version: 2.2</b><br />
<b>Updated: December 2011</b><br /><br />
<p><b>Redirect URL</b> - This is the destination web address URL that you want to redirect your mobile visitors too. (i.e. http://m.jumpmobi.com)</p>
<p><b>Detection Type</b> - Choose between PHP User Agent Detection or Javascript Screen Size Detection</p>
<p><b>Set Screen Size Detection</b> - If using Javascript Detection any visitors with a pixel screen size LESS THAN this number will be redirected to your redirect URL, 1024 is recommended. Do not include px. Use higher sizes for testing purposes ONLY.</p>
<p><b>IFrame Detection</b> - If enabled your site will initiate the redirect if your site is loaded into an IFrame, for example if your site is loaded into a web based mobile simulator</p>
<p><b>Popup Alert</b> - If enabled your mobile visitor will be prompted to choose to be redirected or not. <b>Note: Compatible with Javascript Detection Option Only</b></p>
<p><b>Popup Alert Text</b> - If the Popup Alert option is enabled you can modify the text that is displayed in the pop up alert box. <b>Note: Compatible with Javascript Detection Option Only</b></p>
<p><b>Redirect Returning Visitors</b> - If enabled previously redirected mobile visitors who return to your site within 15 minutes will not be redirected again. <b>Note: To test you will have to delete cookies each time you change your settings</b></p>
<p><b>Please note in some cases the javascript features will NOT work if you are still logged in as admin - this is to prevent you from being redirected to your mobile site whilst logged in using your mobile device.</b></p>
<p>For further support visit <a href="http://www.jumpmobi.com" target="_blank">JumpMobi.com</a></p>');
?>